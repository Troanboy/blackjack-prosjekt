import java.sql.*;
import java.util.ResourceBundle;

public class DBConnect {
    static Connection con;
    static private int startbalance = 10000;
    static ResourceBundle jdbc_resource;
    DBConnect() {
        jdbc_resource = ResourceBundle.getBundle("DBConnect");
       String myDriver = jdbc_resource.getString("jdbc.driver");
       try{
           Class.forName(myDriver);
           System.out.println("Driver loaded");
       }
       catch(ClassNotFoundException ee){
           System.out.println("Driver not loaded");
       }
       String myUrl = jdbc_resource.getString("jdbc.url");


       try{
           con = DriverManager.getConnection(myUrl);  //Connecting with the database
       }catch (SQLException ee){
           ee.printStackTrace();
       }
    }

   public boolean logInDB(String userName,String password){   //Returns true if username and password is correct
       ResultSet rs;
       try{
           PreparedStatement st = (PreparedStatement) con.prepareStatement("Select Username, Password from Persons where Username=? and Password=?");
           st.setString(1, userName);   //Inserting userName in the prepared statement
           st.setString(2, password);   //Inserting password in the prepared statement
           rs = st.executeQuery();                    //Executing the prepared statement
           if (rs.next()) {                         //If the username and password is correct
               return true;

           } else {        //else if the user or password is not correct
               return false;
           }

       }catch(SQLException ee){
           ee.printStackTrace();
           return false;
       }

   }

   public int getBalance(String userName){
       ResultSet rs;
       try{
           PreparedStatement st = (PreparedStatement) con.prepareStatement("Select Balance from Persons where Username=?");
           st.setString(1, userName);   //Inserting userName in the prepared statement
           rs = st.executeQuery();  //Executing the query
           if(rs.next()) {                            //If the username is not in the loggedInUsers table
               int balance = rs.getInt("Balance");  //Get the balance of the user
               return balance;
           }else{
               return 0;
           }

       }catch(SQLException ee){
           ee.printStackTrace();
           return 0;
       }
   }

   public void insertIntoLoggedInUsers(String userName){
        try{
            PreparedStatement st = (PreparedStatement) con.prepareStatement("Insert into loggedInUsers (Username) values (?);");
            st.setString(1, userName);
            st.execute();  //Executing a query that inserts the username into loggedInUsers table
        }catch(SQLException ee){
            ee.printStackTrace();

        }
   }

   public boolean isUserLoggedIn(String userName){
       ResultSet rs;
       try{
            PreparedStatement st = (PreparedStatement) con.prepareStatement("Select Username from loggedInUsers where Username=?");
            st.setString(1, userName);   //Inserting userName in the prepared statement
            rs = st.executeQuery();  //Executing the query
            if(rs.next()) {                            //If the username is not in the loggedInUsers table
                return true;
            }else {
                return false;
            }

        }catch(SQLException ee){
            ee.printStackTrace();
            return true;
        }
   }

   public boolean doesUserExist(String userName){
       try{
           ResultSet rs;

           PreparedStatement st = (PreparedStatement) con.prepareStatement("Select Username from Persons where Username=?");
           st.setString(1, userName);   //Inserting userName in the prepared statement
           rs = st.executeQuery();                    //Executing the prepared statement
           if (rs.next()) {                         //If the username is used
               return true;
           } else {
               return false;
           }
       }catch(SQLException ee){
           ee.printStackTrace();
           return true;
       }
   }

   public void createNewUser(String userName, String password){
       try{
           PreparedStatement st = (PreparedStatement) con.prepareStatement("INSERT INTO Persons (Username, [Password], Balance) VALUES (?, ?, ?);");
           st.setString(1, userName);   //Inserting userName in the prepared statement
           st.setString(2, password);   //Inserting password in the prepared statement
           st.setInt(3, startbalance);   //Inserting balance in the prepared statement
           st.execute();
       }catch(SQLException ee){
           ee.printStackTrace();
       }
   }

   public void removeUserFromLoggedInTable(String userName){
        try{
            PreparedStatement st = (PreparedStatement) con.prepareStatement("DELETE FROM loggedInUsers WHERE Username = ?;");
            st.setString(1, userName);
            st.execute();  //Executing a query that deletes the user from loggedInUsers-table
        }catch(SQLException ee){
            ee.printStackTrace();
        }
   }

   public void changePassword(String userName, String password){
        try{
            PreparedStatement st = (PreparedStatement) con.prepareStatement("UPDATE Persons " +
                    "SET Password = ? " +
                    "WHERE Username = ?;");
            st.setString(1, password);   //Inserting balance e in the prepared statement
            st.setString(2, userName);   //Inserting username in the prepared statement
            st.execute();
        }catch(SQLException ee){
            ee.printStackTrace();
        }

   }

   public void changeBalance(String userName, int newBalance){
        try{
            PreparedStatement st = (PreparedStatement) con.prepareStatement("UPDATE Persons " +
                    "SET Balance = ? " +
                    "WHERE Username = ?;");
            st.setInt(1, newBalance);   //Inserting balance e in the prepared statement
            st.setString(2, userName);   //Inserting username in the prepared statement
            st.execute();
        }catch(SQLException ee){
            ee.printStackTrace();
        }
   }



}