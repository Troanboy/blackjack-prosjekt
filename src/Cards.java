import javax.swing.*;
import java.io.Serializable;
import java.lang.Math;
import java.util.Arrays;

public class Cards {
    //Array of unused cards in the four decks
    boolean[][][] newCards;
    ImageIcon[][] cards;

    String[] cardTypes = new String[]{"H","D","C","S"};

    //Constructor initializes four deck of cards and loads icons
    Cards() {
        newCards = new boolean[4+1][4+1][14+1];  //4 decks, 4 Colors, (2->13)+ACE = 14
        newRound();
        loadCards();
    }

    public Card drawCard(){
        int[] index = new int[3];
        System.out.println("Draws Card");
        //Fetch a random card in a random deck
        do{
            index[0] = getRandom(1,4);
            index[1] = getRandom(1,4);
            index[2] = getRandom(2,14);
        } while(!newCards[index[0]][index[1]][index[2]]);

        //Card is now used:
        newCards[index[0]][index[1]][index[2]] = false;

        return new Card(index[1],index[2],cards);
    }

    private int getRandom(int min, int max){
        return ((int)(Math.random()*(max-min+1)+min));
    }


    //Renew the deck of cards, make them all unused
    //Also resets unused indexes
    public void newRound(){
        for(int i = 0; i<=4;i++)
            for(int j = 0; j<=4;j++)
                for(int k = 0; k<=14;k++)
                    newCards[i][j][k] = true;
    }

    //Get cards from Resource folder
    private void loadCards(){
        String symbol;
        cards = new ImageIcon[4+1][14+1];

        for (int i = 1; i <= 4; i++) {
            symbol = cardTypes[i - 1];
            for (int j = 2; j <= 14; j++) {
                //For testing, first  line gave an error
                cards[i][j] = new ImageIcon(this.getClass().getResource("Resources/"+ j + symbol + ".png"));
                //cards[i][j] = new ImageIcon(this.getClass().getResource("Resources/2S.png"));
            }
        }
    }

    public ImageIcon loadBackside(){
        ImageIcon backside = new ImageIcon();
        backside = new ImageIcon(this.getClass().getResource("Resources/gray_back.png"));
        return backside;
    }


}

//Class for a single card, used for returning both face and value
class Card implements Serializable {
    public ImageIcon face;
    public int value;

    Card(int cardSymbol, int cardValue, ImageIcon[][] cards){
        face = cards[cardSymbol][cardValue];

        if(cardValue>10 && cardValue<14)
            value = 10;
        else if(cardValue == 14)
            value = 11;
        else
            value = cardValue;
    }
}
