import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.Locale;
import java.util.ResourceBundle;

public class CreateNewUser {
    static JFrame frame;
    private JTextField textField;
    private JPasswordField passwordField;
    static JButton newUserButton;
    static JButton loginButton;
    static JLabel label;
    static JLabel lblUsername;
    static JLabel lblNewLabel;
    static JLabel lblPassword;
    private JPanel screenPanel;
    private int startbalance = 10000;
    Home mainscreen = new Home();
    static ResourceBundle createUser;
    static String lang = "en";
    static Boolean created = false;


    public void CreateNewUser() {
        createUser = ResourceBundle.getBundle("login", Locale.forLanguageTag(lang));
        ImageIcon backGroundimg = new ImageIcon(this.getClass().getResource("Resources/background_green.jpg"));

        frame = new JFrame(createUser.getString("titleNewUser") + ": BlackJack");
        ImageIcon logoIcon = new ImageIcon(this.getClass().getResource("Resources/defaultIcon.png"));
        Image img = logoIcon.getImage();
        frame.setIconImage(img);
        frame.setSize(700, 597);
        // frame.setLayout(new BorderLayout());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setJMenuBar(Menubar.Menubar(frame, lang, null));

        JLayeredPane Layeredpane = new JLayeredPane();             //Make a new layeredpane

        JLabel backGround = new JLabel();
        backGround.setBounds(0, 0, 700, 597);
        backGround.setIcon(backGroundimg);
        Layeredpane.add(backGround, JLayeredPane.DEFAULT_LAYER);   //Add background behind the components


        lblNewLabel = new JLabel(createUser.getString("titleNewUser"));          //Title label
        lblNewLabel.setForeground(Color.lightGray);
        lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 46));
        lblNewLabel.setBounds(150, 13, 400, 93);
        Layeredpane.add(lblNewLabel, JLayeredPane.POPUP_LAYER);


        label = new JLabel("");                  //Label to give instruction messages
        label.setForeground(Color.lightGray);
        label.setBackground(Color.lightGray);
        label.setBounds(93,90,300,50);
        Layeredpane.add(label, JLayeredPane.POPUP_LAYER);

        lblUsername = new JLabel(createUser.getString("username"));       //Username label
        lblUsername.setBackground(Color.lightGray);
        lblUsername.setForeground(Color.lightGray);
        lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 31));
        lblUsername.setBounds(93, 130, 193, 52);
        Layeredpane.add(lblUsername, JLayeredPane.POPUP_LAYER);


        textField = new JTextField();                //Textfield to insert the username
        textField.setFont(new Font("Tahoma", Font.PLAIN, 32));
        textField.setFont(new Font("Tahoma", Font.PLAIN, 32));
        textField.setBounds(324, 134, 281, 68);
        Layeredpane.add(textField, JLayeredPane.POPUP_LAYER);

        lblPassword = new JLabel(createUser.getString("password"));    //Password Label
        lblPassword.setForeground(Color.lightGray);
        lblPassword.setBackground(Color.lightGray);
        lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 31));
        lblPassword.setBounds(93, 200, 193, 52);
        Layeredpane.add(lblPassword, JLayeredPane.POPUP_LAYER);

        passwordField = new JPasswordField();      //Textfield to insert the password
        passwordField.setFont(new Font("Tahoma", Font.PLAIN, 32));
        passwordField.setFont(new Font("Tahoma", Font.PLAIN, 32));
        passwordField.setBounds(324, 204, 281, 68);
        Layeredpane.add(passwordField, JLayeredPane.POPUP_LAYER);


        newUserButton = new JButton(createUser.getString("newUser"));   //Button to login with the username and password
        newUserButton.setFont(new Font("Tahoma", Font.PLAIN, 26));
        newUserButton.setBackground(Color.LIGHT_GRAY);
        newUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String userName = textField.getText();
                String password = passwordField.getText();
                createUserDB(userName, password);
            }
        });
        newUserButton.setBounds(324, 270, 281, 73);
        Layeredpane.add(newUserButton, JLayeredPane.POPUP_LAYER);


        loginButton = new JButton(createUser.getString("backLogin"));   //Button to login as a guest user
        loginButton.setFont(new Font("Tahoma", Font.PLAIN, 26));
        loginButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                Login l = new Login();
                l.Login();

            }
        });
        loginButton.setBounds(93,390, 281, 73);
        Layeredpane.add(loginButton, JLayeredPane.POPUP_LAYER);

        frame.add(Layeredpane);
        created = true;
        frame.setVisible(true);
    }

    static void updateGUI(String s) {
        createUser = ResourceBundle.getBundle("login", Locale.forLanguageTag(s));
        newUserButton.setText(createUser.getString("newUser"));
        loginButton.setText(createUser.getString("backLogin"));
        lblPassword.setText(createUser.getString("password"));
        lblUsername.setText(createUser.getString("username"));
        lblNewLabel.setText(createUser.getString("newUser"));
        frame.setTitle(createUser.getString("titleNewUser") + ": BlackJack");
    }

   private void createUserDB(String userName, String password){
       DBConnect db = new DBConnect();
       if(!db.doesUserExist(userName)){            //Check if the username is taken
           db.createNewUser(userName, password);  //Creates a new user with the given username and password
           db.insertIntoLoggedInUsers(userName);  //Insert the userName into loggedInUsers table
           frame.dispose();
           Player player = new Player(userName, password, startbalance);
           mainscreen.RunHome(player);   //Homescreen with player object
       }else {
           label.setText(createUser.getString("usedUsername"));
       }
   }
}
