//Server for the chat

import javax.swing.*;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.util.ArrayList;

public class Server {
    private JFrame frame;
    private JTextArea outputArea;

    ArrayList<ServerConnection> connections = new ArrayList<ServerConnection>();  //list of connections
    private ServerSocket server;
    private int currentConnection;
    private int numberOfPlayers = 0;
    private final int MAXPLAYERS = 3;   //Maximum number of players

    public  Server(){
        outputArea = new JTextArea();
        frame = new JFrame("chat server");
        frame. setSize(500, 500);
        frame.setVisible(true);
        frame.add(outputArea, BorderLayout.CENTER);
        display("Server awaiting connections...");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        try {
            server = new ServerSocket(5000, 2);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    //executes server, waiting for connections
    public void execute(int j){                                                   //parameter that tells how many players are currently playing
        display("going through a for loop from " + numberOfPlayers + "players");
        System.out.println(Thread.currentThread().getName()+" IN EXECUTE");
        while(true){                                                               //listening for connections while true
            try {
                Socket s = server.accept();                                        //accepting socket
                ServerConnection sc = new ServerConnection(s, numberOfPlayers);    //creates a new serverConnection object
                connections.add(sc);                                               //adds object to list
                display("server connected to connection number " + connections.get(numberOfPlayers).playerNumber);
                numberOfPlayers++;                                                 //Incrementing total number of players
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

        }

    }

    //Displays info in server console
    public void display(String text){    //displays text to the server
        outputArea.append(text + "\n");
    }

    //ServerConnection class for each individual connection
    private class ServerConnection extends Thread{
        private Socket connection;                  //connection to client
        private DataInputStream input;
        private DataOutputStream output;
        public int playerNumber;                    //Clients current playerNumber
        public boolean shouldRun = true;

        public ServerConnection(Socket socket, int number){
            connection = socket;
            playerNumber = number;

            try {
                input = new DataInputStream(connection.getInputStream());
                output = new DataOutputStream(connection.getOutputStream());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            start();                               //runs thread (calls run function)
        }

        public void run(){
            display("player "+ playerNumber + " connected");
            try{

                while(shouldRun){                                 //listens to input from client while true
                    String textIn = input.readUTF();
                    display("CLIENT " + playerNumber + " - " + textIn);   //displays on console
                    sendStringToAllClients(textIn);                            //sends message to all other clients
                }

                input.close();
                output.close();
                connection.close();
            }
            catch(IOException e){
                e.printStackTrace();
            }

        }

        //sends message to one client
        public void sendStringToClient(String text){
            try {
                output.writeUTF(text);
                output.flush();
            } catch (IOException ioException){
                ioException.printStackTrace();
            }
        }
        //Sends message to all clients
        public void sendStringToAllClients(String text){
            for(int j = 0;j<connections.size();j++){
                connections.get(j).sendStringToClient(text);
            }
        }

    }
}
