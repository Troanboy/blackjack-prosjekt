import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class Singleplayer {
    PlayerPanel player;                           //Set variables
    PlayerPanel dealerPanel;
    Client message;
    Font calibri;
    JLayeredPane Layeredpane;
    static JLabel info;
    static JLabel PlayerName;
    static JLabel PlayerBalance;
    ImageIcon realFace;
    Card[] startCards;                  //resets starting cards
    Card[] startCardsDealer;
    private Player user;

    static JButton hit;
    static JButton split;
    static JButton doubleDown;
    static JButton leaveGame;
    static JButton stand;
    static JButton submitBet;
    static JButton newRound;

    static JButton chip5;
    static JButton chip10;
    static JButton chip50;
    static JButton chip100;

    static JFrame frame;
    static String lang = "en";
    static ResourceBundle game;
    static Boolean created = false;

    Cards cards;
    Card card;
    Boolean dealerHasAce = false;
    Boolean playerHasAce = false;
    Boolean handDone1 = false;
    int splitBet = 0;
    int totalWin = 0;



    //Resizes icon to fit label
    private static ImageIcon resizeIcon(ImageIcon icon, int Width, int Height) {
        Image img = icon.getImage();                                                   //Makes image object
        Image resizedIcon = img.getScaledInstance(Width, Height, java.awt.Image.SCALE_SMOOTH);  //Scales image
        return new ImageIcon(resizedIcon);                                                      //returns resized imageobject
    }

    public void start(Player user) {
        Layeredpane = new JLayeredPane();             //Make a new layeredpane
        calibri = new Font(Font.DIALOG_INPUT, Font.BOLD, 25);     //Declares new font
        this.user = user;
        cards = new Cards();
        game = ResourceBundle.getBundle("game", Locale.forLanguageTag(lang));
        frame = new JFrame(game.getString("titleSP"));
        ImageIcon logoIcon = new ImageIcon(this.getClass().getResource("Resources/defaultIcon.png"));
        Image img = logoIcon.getImage();
        frame.setIconImage(img);
        frame.setJMenuBar(Menubar.Menubar(frame, lang, user.getUserName()));//Sets menubar from manubar class
        frame.getContentPane().setBackground(Color.green);
        frame.setSize(1000, 600);
        frame.setVisible(true);
        //frame.setLayout(null);
        frame.getContentPane().setBackground(Color.DARK_GRAY);


        //Panel for user of GUI
        player = new PlayerPanel("Player");
        player.getPanel().setBounds(375, 370, 300, 215);


        //DealerPanel
        dealerPanel = new PlayerPanel("Dealer");
        dealerPanel.getPanel().setBounds(375, 10, 300, 170);


        //Background
        ImageIcon background = new ImageIcon(this.getClass().getResource("Resources/background.png"));
        JLabel contentPane = new JLabel();
        contentPane.setBounds(40, 0, 900, 600);
        contentPane.setIcon(resizeIcon(background, contentPane.getWidth(), contentPane.getHeight()));


        info = new JLabel(game.getString("welcome"), SwingConstants.CENTER);                           //Declare infolabel
        info.setBounds(350, 320, 300, 35);
        info.setFont(calibri);
        info.setForeground(Color.YELLOW);


        PlayerName = new JLabel(game.getString("username") + (": ") + user.getUserName());
        PlayerName.setBounds(810,20,150,30);
        PlayerName.setForeground(Color.YELLOW);

        PlayerBalance = new JLabel(String.valueOf(game.getString("balance") + (": ") + user.getBalance()));
        PlayerBalance.setBounds(810,40,100,30);
        PlayerBalance.setForeground(Color.YELLOW);




        JLayeredPane Layeredpane = new JLayeredPane();             //Make a new layeredpane

        hit = new JButton(game.getString("hit"));                           //Declares buttons
        split = new JButton(game.getString("split"));
        doubleDown = new JButton(game.getString("double"));
        leaveGame = new JButton(game.getString("leaveGame"));
        stand = new JButton(game.getString("standTitle"));
        submitBet = new JButton(game.getString("submit"));
        newRound = new JButton(game.getString("newRound"));
        submitBet.setEnabled(false);
        newRound.setEnabled(false);
        split.setEnabled(false);

        leaveGame.setBounds(10,530, 140,50);              //Place buttons on frame
        hit.setBounds(810,470,160,50);
        doubleDown.setBounds(810,410,160,50);
        split.setBounds(810,350,160,50);
        stand.setBounds(810,530,160,50);
        submitBet.setBounds(460,350,80,30);
        newRound.setBounds(10,470,140,50);

        chip5=new JButton();                                    //Declares clickable chips
        chip10=new JButton();
        chip50=new JButton();
        chip100=new JButton();
        chip5.setIcon(player.tokens[0]);
        chip10.setIcon(player.tokens[1]);
        chip50.setIcon(player.tokens[2]);
        chip100.setIcon(player.tokens[3]);
        chip5.setBounds(400,390,50,50);
        chip10.setBounds(450,390,50,50);
        chip50.setBounds(490,390,50,50);
        chip100.setBounds(530,390,50,50);
        chip5.setBorder(null);                                      //Showing only the chips icon
        chip10.setBorder(null);
        chip50.setBorder(null);
        chip100.setBorder(null);
        chip5.setContentAreaFilled(false);
        chip10.setContentAreaFilled(false);
        chip50.setContentAreaFilled(false);
        chip100.setContentAreaFilled(false);

        frame.addWindowListener(new WindowAdapter(){  //Makes the user confirm exit and notifies server
            public void windowClosing(WindowEvent e){
                DBConnect db = new DBConnect();
                db.removeUserFromLoggedInTable(user.getUserName());   //Logs out user upon exit
                user.changeBalance(user.getBalance());
                frame.dispose();
            }
        });

        Layeredpane.add(hit, JLayeredPane.POPUP_LAYER);                  //Add buttons in front of background
        Layeredpane.add(split, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(doubleDown, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(leaveGame, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(stand, JLayeredPane.POPUP_LAYER);
        toggleButtons(false);

        Layeredpane.add(contentPane, JLayeredPane.DEFAULT_LAYER);   //Add background behind the components
        Layeredpane.add(submitBet,JLayeredPane.POPUP_LAYER);
        Layeredpane.add(dealerPanel.panel, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(player.panel, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(info, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(newRound, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(PlayerName, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(PlayerBalance, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(player, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(dealerPanel, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(chip5,JLayeredPane.POPUP_LAYER);
        Layeredpane.add(chip10,JLayeredPane.POPUP_LAYER);
        Layeredpane.add(chip50,JLayeredPane.POPUP_LAYER);
        Layeredpane.add(chip100,JLayeredPane.POPUP_LAYER);
        frame.add(Layeredpane);
        frame.setJMenuBar(Menubar.Menubar(frame, lang, user.getUserName()));                          //Sets menubar from manubar class
        frame.setSize(1000, 650);
        created = true;
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        int delay = 2000;                                          //Fixes so user bets after welcome message
        Timer timer = new Timer( delay, new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent e ){
                bet();
            }
        } );
        timer.setRepeats( false );
        timer.start();

        leaveGame.addActionListener(new ActionListener() {           //If leavegame is clicked
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                user.changeBalance(user.getBalance());  //Updates balance to the database
                Home mainscreen = new Home();
                mainscreen.RunHome(user);
            }
        });
        newRound.addActionListener(new ActionListener() {             //If user wants new round
            @Override
            public void actionPerformed(ActionEvent e) {
                newRound();
            }
        });

        hit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                split.setEnabled(false);
                card = cards.drawCard();                          //Draw card
                if (player.split && handDone1){                   //If player have splitted and first hand is doen
                    player.addSplitCardSp(card.face);
                    player.createBorder("SPLIT");
                    player.setHandValSpilt(card.value);
                    convertAceSplit(card);
                    player.changeHandValueSplit(player.getHandValSplit());
                    if(player.handValSplit > 21) {                           //If second hand busted
                        toggleButtons(false);
                        newRound.setEnabled(true);
                        player.changeMessageSplit("Bust!");
                        player.createBorder("RESET");
                        dealerMove();
                        checkWinner();
                    }
                }
                else if (player.split){                              //If player have splitted
                    player.addCard(card);
                    convertAce(card);
                    player.createBorder("FIRST");
                    if (player.handVal > 21) {                       //If firsthand have busted
                        handDone1 = true;
                        player.createBorder("SPLIT");
                    }
                }
                else {                                           //Not splitted
                    player.addCard(card);
                    convertAce(card);
                    if(player.getHandVal() > 21){                 //If busted
                        info.setText(game.getString("bust"));
                        player.bust = true;
                        dealerPanel.changeBack(startCardsDealer[1]);
                        dealerPanel.changeHandValue(dealerPanel.getHandVal());
                        split.setEnabled(false);
                        toggleButtons(false);
                        newRound.setEnabled(true);
                    }
                    else if (player.getHandVal() == 21){         //If player gets 21
                        toggleButtons(false);
                        dealerMove();
                        checkWinner();
                    }
                    else{
                        split.setEnabled(false);
                        doubleDown.setEnabled(false);
                    }
                }
            }
        });
        doubleDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                split.setEnabled(false);
                card = cards.drawCard();
                player.addBet(player.getBet());                   //Doubles the chips and the bet
                player.setBet(player.getBet());

                player.addCard(card);
                convertAce(card);
                if (player.getHandVal() > 21) {                  //If busted
                    info.setText(game.getString("bust"));
                    toggleButtons(false);
                    checkWinner();
                    newRound.setEnabled(true);
                }
                else {                                         //If not busted
                    toggleButtons(false);
                    dealerMove();
                    checkWinner();
                }
                int temp = user.getBalance() - (player.getBet() / 2);        //Sets balance
                user.setBalance(temp);
                PlayerBalance.setText(String.valueOf(temp));
            }
        });
        split.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                split.setEnabled(false);
                doubleDown.setEnabled(false);

                player.splitSetup();                        //removes one card
                player.addSplitCardSp(startCards[1].face);    //and adds it above
                user.setBalance(user.getBalance() - player.getBet());   //doubles bet


                splitBet = player.currentBet;                           //splitbet equals startbet
                player.setBet(player.getBet() + splitBet);

                PlayerBalance.setText(String.valueOf(user.getBalance()));

                player.handVal = startCards[0].value;
                player.changeHandValue(player.handVal);
                player.handValSplit += startCards[1].value;
                player.changeHandValueSplit(player.handValSplit);
                player.split = true;

                player.createBorder("FIRSTHAND");
                if (startCards[0].value == 11 && startCards[1].value == 11){      //If gets two aces draws two cards
                    player.createBorder("RESET");
                    card = cards.drawCard();
                    player.addCard(card);
                    convertAce(card);
                    card = cards.drawCard();
                    player.addSplitCardSp(card.face);
                    convertAce(card);
                    player.handValSplit += card.value;
                    player.changeHandValueSplit(player.handValSplit);
                    toggleButtons(false);
                    dealerMove();
                    checkWinner();
                }
            }
        });
        stand.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!player.split){                            //if not splitted
                    dealerMove();
                    checkWinner();
                    toggleButtons(false);
                    split.setEnabled(false);
                }
                else if (!handDone1) {                       //If not first hand is done
                    handDone1 = true;
                    player.createBorder("SPLIT");
                }
                else {
                    dealerMove();
                    checkWinner();
                    toggleButtons(false);
                }

            }
        });
        toggleButtons(false);
    }




    private void bet() {                                       //Adds bet if chips is clicked and balance is enough
        showBets(true);
        info.setText(game.getString("placeBets"));
        chip5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if((user.getBalance() - player.getBet()) >=5) {
                    submitBet.setEnabled(true);
                    player.addBet(5);
                }
            }
        });
        chip10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if((user.getBalance() - player.getBet()) >=10) {
                    submitBet.setEnabled(true);
                    player.addBet(10);
                }
            }
        });
        chip50.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if((user.getBalance() - player.getBet()) >=50) {
                    submitBet.setEnabled(true);
                    player.addBet(50);
                }
            }
        });
        chip100.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if((user.getBalance() - player.getBet()) >=100) {
                    submitBet.setEnabled(true);
                    player.addBet(100);
                }
            }
        });
        submitBet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showBets(false);
                int temp = user.getBalance() - player.getBet();
                PlayerBalance.setText(String.valueOf(temp));
                user.setBalance(temp);
                play();
            }
        });
    }


    private void play(){
        dealerHasAce=false;
        playerHasAce=false;

        toggleButtons(true);
        info.setText(game.getString("drawsCards"));
        submitBet.setVisible(false);
        startCards = getStartingCards();                //Get start cards
        player.addCard(startCards[0]);                         //Add to panel
        player.addCard(startCards[1]);
        convertAce(startCards[0]);                      //Converts startcards
        convertAce(startCards[1]);
        startCardsDealer = getStartingCards();
        dealerPanel.addCard(startCardsDealer[0]);
        dealerPanel.addCard(startCardsDealer[1]);
        dealerPanel.changeFaceStartCard(startCardsDealer[1]);                 //makes second card not visible to player
        dealerPanel.changeHandValue(startCardsDealer[0].value);



        if(startCardsDealer[0].value == 11 || startCardsDealer[1].value == 11)       //If dealer gets ace
            dealerHasAce = true;

        if(startCards[0].value == 11 || startCards[1].value == 11)       //If player gets ace
            playerHasAce = true;

        info.setText("");
        if (startCards[0].value == startCards[1].value)                //If player gets same cards
            split.setEnabled(true);
        if (player.getBet()  > user.getBalance()){                    //Cannot double bet if not enough balance
            split.setEnabled(false);
            doubleDown.setEnabled(false);
        }



        if(dealerPanel.handVal==21){                               //If dealer gets blackjack
            checkWinner();
            toggleButtons(false);
            split.setEnabled(false);
            dealerPanel.changeMessage("Blackjack!");
            newRound.setEnabled(true);

        }
        if(player.handVal==21){                           //If player gets blackjack
            checkWinner();
            toggleButtons(false);
            split.setEnabled(false);
            player.changeMessage("Blackjack!");
            newRound.setEnabled(true);
        }

        if(playerHasAce && player.handVal<21) {
            player.changeMessage("");
            player.changeHandValue(player.getHandVal());
        }

        if(dealerHasAce && dealerPanel.handVal<21) {
            dealerPanel.changeMessage("");
            dealerPanel.changeHandValue(startCardsDealer[0].value);
        }
    }



    private void convertAce(Card card){
        if(card.value == 11)                         //If player has ace
            playerHasAce = true;
        if(player.getHandVal()>21 && playerHasAce) {       //Subtract 10 if handvalue is over 21
            player.handVal -= 10;
            playerHasAce = false;
            player.changeMessage("");
            player.changeHandValue(player.getHandVal());
        }
    }

    private void convertAceSplit(Card card){
        if(card.value == 11)                         //If player has ace
            playerHasAce = true;
        if(player.getHandValSplit()>21 && playerHasAce) {       //Subtract 10 if handvalue is over 21
            player.handValSplit -= 10;
            playerHasAce = false;
            player.changeMessageSplit("");
            player.changeHandValueSplit(player.getHandValSplit());
        }
    }

    public Card dealerMove(){                       //What dealer should do

        while(dealerPanel.handVal < 17) {
            card = cards.drawCard();
            if(card.value == 11)       //If dealer gets ace
                dealerHasAce = true;
            dealerPanel.addCard(card);
            if(dealerHasAce && dealerPanel.handVal<21)
                dealerPanel.changeMessage("");
        }

        if(dealerHasAce && dealerPanel.handVal == 17){            //If dealer has soft hand
            card = cards.drawCard();
            dealerPanel.addCard(card);
        }
        if(dealerHasAce && dealerPanel.handVal>21) {
            dealerPanel.handVal -= 10;    //Ace goes from 11->1 if overshoots
            dealerHasAce = false;
            dealerMove();                                              //runs again recursive if gets an ace
        }
        if (dealerPanel.handVal > 21) {
            dealerPanel.changeMessage("Bust");
        }
        dealerPanel.changeBack(startCardsDealer[1]);
        dealerPanel.changeHandValue(dealerPanel.getHandVal());
        return card;
    }

    public void checkWinner(){                                        //Checks who won
        dealerPanel.changeBack(startCardsDealer[1]);                   //Turn card back
        dealerPanel.changeHandValue(dealerPanel.getHandVal());
        if(player.split){
            player.createBorder("RESET");
            if (player.usedSplitCards == 2 && player.handValSplit == 21){                //If second hand gets blackjack
                player.changeMessageSplit("Blackjack");
                user.setBalance(user.getBalance() + (splitBet * 2 + splitBet/2));
                PlayerBalance.setText(String.valueOf(user.getBalance()));
                totalWin += (splitBet + splitBet/2);
            }
            else if (player.handValSplit > 21) {                                //If second hand bust
                player.changeMessageSplit(game.getString("bust"));
                totalWin += -splitBet;
            }
            else if (dealerPanel.handVal > 21) {                             //If dealer busts
                player.changeMessageSplit(game.getString("win"));
                user.setBalance(user.getBalance() + splitBet*2);
                PlayerBalance.setText(String.valueOf(user.getBalance()));
                totalWin += splitBet;
            }
            else if (dealerPanel.handVal > player.handValSplit) {                 //If dealer gets closest
                player.changeMessageSplit(game.getString("dealerW"));
                totalWin += -splitBet;
            }
            else if (dealerPanel.handVal < player.handValSplit) {              //If player gets closest
                player.changeMessageSplit(game.getString("win"));
                user.setBalance(user.getBalance() + splitBet*2);
                PlayerBalance.setText(String.valueOf(user.getBalance()));
                totalWin += splitBet;
            }
            else if (dealerPanel.handVal == player.handValSplit){             //If push
                player.changeMessageSplit(game.getString("tie"));
                user.setBalance(user.getBalance() + splitBet);
                totalWin += 0;
                PlayerBalance.setText(String.valueOf(user.getBalance()));
            }
        }

        if (player.usedCards == 2 && player.handVal == 21) {             //If first hand gets blackjack
            player.changeMessage("Blackjack");
            user.setBalance(user.getBalance() + (player.getBet() * 2 + player.getBet()/2));
            PlayerBalance.setText(String.valueOf(user.getBalance()));
            totalWin += (player.getBet() + player.getBet()/2);
            newRound.setEnabled(true);
        }
        else if(player.getHandVal() > 21) {                           //If player busts
            player.changeMessage(game.getString("bust"));
            totalWin += -player.getBet();
            newRound.setEnabled(true);
        }
        else if (dealerPanel.getHandVal() > 21){                    //If dealer busts
            player.changeMessage(game.getString("win"));
            user.setBalance(user.getBalance() + (2 * player.getBet()));
            PlayerBalance.setText(String.valueOf(user.getBalance()));
            totalWin += player.getBet();
            newRound.setEnabled(true);
        }
        else if (player.getHandVal() > dealerPanel.getHandVal()) {          //If player is closest
            player.changeMessage(game.getString("win"));
            user.setBalance(user.getBalance() + (2 * player.getBet()));
            PlayerBalance.setText(String.valueOf(user.getBalance()));
            totalWin += player.getBet();
            newRound.setEnabled(true);
        }
        else if (player.getHandVal() < dealerPanel.getHandVal()) {            //If dealer is closest
            player.changeMessage(game.getString("dealerW"));
            totalWin += -player.getBet();
            newRound.setEnabled(true);
        }
        else if (player.getHandVal() == dealerPanel.getHandVal()) {        //If push
            player.changeMessage(game.getString("tie"));
            user.setBalance(user.getBalance() + player.getBet());
            PlayerBalance.setText(String.valueOf(user.getBalance()));
            totalWin += 0;
            newRound.setEnabled(true);
        }

        if(totalWin > 0) {                                                 //Displays info to player
            info.setText(game.getString("win") + (" ") + totalWin);
        }
        else if(totalWin == 0) {
            info.setText(game.getString("win") + (" ") + totalWin);
        }
        else {
            info.setText(game.getString("loss") + (" ") + Math.abs(totalWin));
        }
    }

    public void newRound(){                            //Creates new round
        if(user.getBalance()<5){                       //Kicks the player out if balance is lower than 5
            user.changeBalance(user.getBalance());
            frame.dispose();
            Home mainscreen = new Home();
            mainscreen.RunHome(user);
        }
        newRound.setEnabled(false);
        split.setEnabled(false);
        user.setBalance(user.getBalance());
        info.setText(game.getString("newRound"));
        player.createBorder("RESET");
        player.reset();                                 //Resets panels
        dealerPanel.reset();
        showBets(true);
        handDone1 = false;
        splitBet = 0;
        submitBet.setEnabled(false);
        totalWin = 0;
    }

    private void showBets(boolean state){                  //Show chips
        chip5.setVisible(state);
        chip10.setVisible(state);
        chip50.setVisible(state);
        chip100.setVisible(state);
        submitBet.setVisible(state);
    }

    public Card[] getStartingCards() {                          //Get two startings cards
        Card[] starterCards = new Card[2];
        starterCards[0] = cards.drawCard();
        starterCards[1] = cards.drawCard();
        return starterCards;
    }

    private void toggleButtons(boolean state) {                       //enable/disable buttons
        hit.setEnabled(state);
        doubleDown.setEnabled(state);
        stand.setEnabled(state);
    }

    static void updateGUI (String s){                                    //Updates language of components
        game = ResourceBundle.getBundle("game", Locale.forLanguageTag(s));
        frame.setTitle(game.getString("titleSP"));
        hit.setText(game.getString("hit"));
        split.setText(game.getString("split"));
        doubleDown.setText(game.getString("double"));
        leaveGame.setText(game.getString("leaveGame"));
        newRound.setText(game.getString("newRound"));
        submitBet.setText(game.getString("submit"));
    }
}