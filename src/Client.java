//Chat client

import javax.swing.*;
import java.io.*;
import java.net.*;
import java.awt.event.*;

public class Client implements Runnable {
    private JPanel panel;
    private JTextArea chatWindow;
    private JTextField userText;
    private DataInputStream input;       //input from server
    private DataOutputStream output;         //output to server
    private String message = "";
    private String serverIP;
    private Socket connection;
    private Thread outputThread;
    private String text;
    private boolean shouldRun = true;
    private Player player;


    public Client(String host, Player player){
        this.player = player;
        chatWindow = new JTextArea();
        chatWindow.setEditable(false);
        userText = new JTextField();
        panel = new JPanel();
        panel.setLayout(null);
        serverIP=host;

        userText.addActionListener(new ActionListener() {  //listen to user input
            @Override
            public void actionPerformed(ActionEvent e) {
                sendMessage(e.getActionCommand());
                userText.setText("");
            }
        });

        userText.setBounds(980,500,200,30);
        userText.setText("Chat");
        JScrollPane scrollPane=  new JScrollPane(chatWindow);
        scrollPane.setBounds(980,10,200,480);
        panel.add(userText);
        panel.add(scrollPane);
        panel.setSize(500,300);
        panel.setVisible(true);
    }

    //starts running and initiates connection
    public void startRunning(){
        try {
            showMessage("attempting connection..");
            connection = new Socket(InetAddress.getByName(serverIP), 5000);
            showMessage("you are connected to:" + connection.getInetAddress().getHostName());
            input = new DataInputStream(connection.getInputStream());
            output = new DataOutputStream(connection.getOutputStream());
            output.flush();
            showMessage("connection now set up!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        outputThread = new Thread(this);
        outputThread.start();
    }

    public void stopRunning(){
        shouldRun = false;
    }


    public void run(){
        while(shouldRun){    //Runs while client is connected
            try {
                if(this.input!=null) {
                    text = input.readUTF();     //keeps listening for input
                    showMessage(text);
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        try {              //closing connection
            input.close();
            output.close();
            connection.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }


    }

    public JPanel getPanel(){
        return panel;
    }

    //displays message to user
    private void showMessage(final String text){
        SwingUtilities.invokeLater(new Runnable() {           //A thread that updates the GUI
            @Override
            public void run() {
                chatWindow.append(text + "\n");                         //Adds the message to chatwindow
            }
        });
    }

    //send message to server
    private void sendMessage(String message){
        try{
            output.writeUTF(player.getUserName() + " - " + message);     //Sends message
            output.flush();                              //Empties buffers and sends header information
        }catch (IOException ioException){
            chatWindow.append("\n ERROR: Cant send message");
        }
    }
}
