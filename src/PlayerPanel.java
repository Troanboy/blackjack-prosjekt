import com.sun.source.tree.WhileLoopTree;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class PlayerPanel extends JPanel {
    final int MAX = 10;   //Max amounts of cards and tokens

    int usedTokens;       //Avoid maxing out tokens or cards
    int usedCards;
    int usedSplitCards;
    int handVal = 0;
    int handValSplit = 0;
    boolean bust = false;
    boolean split = false;

    int currentBet = 0;

    JPanel panel;
    String playerName;

    ImageIcon[] tokens = new ImageIcon[4];
    ImageIcon backSide = new ImageIcon();

    //Icon for testing
    ImageIcon cardIcon = new ImageIcon();

    JLabel[] cardLabel = new JLabel[MAX];
    JLabel[] splitCardLabel = new JLabel[MAX];
    JLabel[] betLabel = new JLabel[MAX];

    JLabel messageLabel = new JLabel();
    JLabel messageLableSplit = new JLabel();
    JLabel handValue = new JLabel();
    JLabel handValueSplit = new JLabel();
    JLabel betValue = new JLabel();

    public void copyCards(PlayerPanel pnl){
        for(int i = 0;i<MAX;i++) {
            cardLabel[i].setIcon(pnl.cardLabel[i].getIcon());
            splitCardLabel[i].setIcon(pnl.splitCardLabel[i].getIcon());
            betLabel[i].setIcon(pnl.betLabel[i].getIcon());
        }
        messageLabel.setText(pnl.messageLabel.getText());
        betValue.setText(pnl.betValue.getText());
        handValue.setText(pnl.handValue.getText());
    }

    PlayerPanel(String name) {
        loadIcons();
        changeName(name);
        constructGUI();
        reset();       //Removes testing values
    }

    public JPanel getPanel(){
        return panel;
    }

    public void setBackside(){
        cardLabel[usedCards].setIcon(backSide);
    }

    public void changeHandValue(int value){
        handValue.setText(playerName+"'s Hand: "+value);
    }

    public void changeHandValueSplit(int value) {
        handValueSplit.setText(playerName + "'s (2) Hand: " + value);
    }

    public void changeName(String s){
        playerName = s;
    }

    public void changeMessage(String s){
        messageLabel.setText(s);
    }

    public void changeMessageSplit(String s){
        messageLableSplit.setText(s);
    }

    public int getBet(){
        return currentBet;
    }

    public int getHandVal() {
        return handVal;
    }

    public void setHandValSpilt(int val) {
        handValSplit += val;
    }

    public int getHandValSplit() {
        return handValSplit;
    }

    public void addCard(Card card) {
        if (usedCards < 10) {
            card.face = resizeIcon(card.face, cardLabel[usedCards]);
            cardLabel[usedCards++].setIcon(card.face);
        }
        handVal += card.value;
        changeHandValue(handVal);

    }

    public void addSplitCard(Card card) {
        if(usedSplitCards<10){
            card.face = resizeIcon(card.face, splitCardLabel[usedSplitCards]);
            splitCardLabel[usedSplitCards++].setIcon(card.face);
        }
        handValSplit += card.value;
        changeHandValueSplit(handValSplit);
    }

    public void addSplitCardSp(ImageIcon card) {
        if (usedSplitCards < 10) {
            card = resizeIcon(card, splitCardLabel[usedSplitCards]);
            splitCardLabel[usedSplitCards++].setIcon(card);
        }
    }

    public void splitSetup() {
        cardLabel[1].setIcon(null);
        cardLabel[1].revalidate();
        usedCards--;
    }

    public void doSplit() {
        splitCardLabel[0].setIcon(cardLabel[1].getIcon());
        cardLabel[1].setIcon(null);
        handValSplit = handVal/2;
        handVal = handVal/2;
        changeHandValue(handVal);
        changeHandValueSplit(handValSplit);
        usedCards--;
        usedSplitCards++;
    }

    public void createBorder(String s) {
        if (s.equals("SPLIT")) {
            for (int i = 0; i < usedSplitCards; i++) {
                splitCardLabel[i].setBorder(BorderFactory.createLineBorder(Color.GREEN, 2));
            }
            for (int j = 0; j < usedCards; j++) {
                cardLabel[j].setBorder(null);
            }
        }
        else if (s.equals("RESET")){
            for (int i = 0; i < usedSplitCards; i++) {
                splitCardLabel[i].setBorder(null);
            }
            for (int j = 0; j < usedCards; j++) {
                cardLabel[j].setBorder(null);
            }
        }
        else {
            for (int i = 0; i < usedCards; i++) {
                cardLabel[i].setBorder(BorderFactory.createLineBorder(Color.GREEN, 2));
            }
        }

    }

    public void changeFaceStartCard(Card card){
        cardLabel[1].setIcon(backSide);
    }

    public void changeBack(Card card){
        card.face = resizeIcon(card.face, cardLabel[1]);
        cardLabel[1].setIcon(card.face);
    }

    //Adds bet and returns the total bet
    public int addBet(int bet){
        if(usedTokens<10) {
            if (bet == 5)
                betLabel[usedTokens++].setIcon(tokens[0]);
            if (bet == 10)
                betLabel[usedTokens++].setIcon(tokens[1]);
            if (bet == 50)
                betLabel[usedTokens++].setIcon(tokens[2]);
            if (bet == 100)
                betLabel[usedTokens++].setIcon(tokens[3]);
        }

        currentBet += bet;
        betValue.setText("BET: " + currentBet);

        return currentBet;
    }

    //Constructs the bet from scratch
    public void setBet(int bet){
        if(bet!=0) {
            usedTokens = 0;
            int rest = 0;
            int tok100 = 0;
            int tok50 = 0;
            int tok10 = 0;
            int tok5 = 0;

            tok100 = bet / 100;
            rest = bet % 100;
            tok50 = rest / 50;
            rest = rest % 50;
            tok10 = rest / 10;
            rest = rest % 10;
            tok5 = rest / 5;
            rest = rest % 5;
            if (rest != 0)
                System.out.println("Invalid bet amount, does not correspond to tokens");

            for (int i = 0; i <= tok100; i++){
                if(usedTokens<10) {
                    betLabel[i].setIcon(tokens[3]);
                    usedTokens++;
                }
            }
            for (int i = tok100; i <= tok50 + tok100; i++) {
                if(usedTokens<10) {
                    betLabel[i].setIcon(tokens[2]);
                    usedTokens++;
                }
            }
            for (int i = tok50 + tok100; i <= tok10 + tok50 + tok100; i++) {
                if(usedTokens<10) {
                    betLabel[i].setIcon(tokens[1]);
                    usedTokens++;
                }
            }
            for (int i = tok10 + tok50 + tok100; i <= tok5 + tok10 + tok50 + tok100; i++) {
                if(usedTokens<10) {
                    betLabel[i].setIcon(tokens[0]);
                    usedTokens++;
                }
            }

        }
        betValue.setText("BET: " + bet);

    }

    //Reset Cards/Bets/Messages
    public void reset(){
        for(int i = 0; i<MAX;i++) {
            cardLabel[i].setIcon(null);
            splitCardLabel[i].setIcon(null);
            betLabel[i].setIcon(null);
        }

        messageLabel.setText("");
        messageLableSplit.setText("");
        handValue.setText("");
        handValueSplit.setText("");
        betValue.setText("");

        usedTokens = 0;
        usedCards = 0;
        usedSplitCards = 0;
        handVal = 0;
        handValSplit = 0;
        currentBet = 0;
        bust = false;
        split = false;
    }

    private void loadIcons(){
        tokens[0] = new ImageIcon(this.getClass().getResource("Resources/5.png"));
        tokens[1] = new ImageIcon(this.getClass().getResource("Resources/10.png"));
        tokens[2] = new ImageIcon(this.getClass().getResource("Resources/50.png"));
        tokens[3] = new ImageIcon(this.getClass().getResource("Resources/100.png"));

        backSide = new ImageIcon(this.getClass().getResource("Resources/gray_back.png"));
        backSide = resizeIcon(backSide, cardLabel[1]);
    }

    private void constructGUI() {

        panel = new JPanel(new GridBagLayout());
        panel.setSize(225,160);
        panel.setOpaque(false);   //Makes the background transparent

        JLayeredPane cardPanel1 = new JLayeredPane();
        JLayeredPane cardPanel2 = new JLayeredPane();
        JLayeredPane betsPanel = new JLayeredPane();
        JPanel messages = new JPanel(new GridBagLayout());

        cardPanel1.setOpaque(false);
        cardPanel2.setOpaque(false);
        betsPanel.setOpaque(false);
        messages.setOpaque(false);

        for(int i = 0; i<MAX;i++){
            cardLabel[i] = new JLabel(cardIcon);
            cardLabel[i].setLocation(i*20,0);
            cardLabel[i].setSize(55,70);
            cardPanel1.add(cardLabel[i],i);
        }

        for(int i = 0; i<MAX;i++){
            splitCardLabel[i] = new JLabel(cardIcon);
            splitCardLabel[i].setLocation(i*20,0);
            splitCardLabel[i].setSize(55,70);
            cardPanel2.add(splitCardLabel[i],i);
        }

        for(int i = 0; i<MAX;i++){
            betLabel[i] = new JLabel(tokens[0]);
            betLabel[i].setLocation(0,i*5);
            betLabel[i].setSize(20,20);
            betsPanel.add(betLabel[i],i);
        }

        GridBagConstraints c = new GridBagConstraints();

        betValue = new JLabel();

        //JUST FOR TESTING:
        handValue.setText(playerName+"'s Hand: 21");
        messageLabel.setText("Win!");
        betValue.setText("BET: 125");
        //JUST FOR TESTING

        handValueSplit.setForeground(Color.WHITE);
        handValue.setForeground(Color.WHITE);
        messageLabel.setForeground(Color.WHITE);
        messageLableSplit.setForeground(Color.WHITE);
        betValue.setForeground(Color.WHITE);

        c.gridy=0;
        messages.add(messageLableSplit,c);
        c.gridy=1;
        messages.add(handValueSplit,c);
        c.gridy=2;
        messages.add(messageLabel,c);
        c.gridy = 3;
        messages.add(handValue, c);


        //(0,0) is currently empty
        c.fill = GridBagConstraints.BOTH;
        c.gridx=0;
        c.gridy=0;

        //(0,1) is bets (chips)
        c.gridx=0;
        c.gridy=1;
        c.weighty = 0.4;
        panel.add(betsPanel,c);

        //(0,2) is bet value
        c.gridx=0;
        c.gridy=2;
        c.weighty = 0;
        betValue.setSize(55,20);
        panel.add(betValue,c);

        //(1,0) is the SPLIT cards
        c.weightx = 0.85;
        c.weighty = 0.3;
        c.gridx=1;
        c.gridy=0;
        panel.add(cardPanel2,c);

        //(1,1) is the main cards
        c.gridx=1;
        c.gridy=1;
        panel.add(cardPanel1,c);

        //(1,2) is the messages
        c.weighty = 0;
        c.weightx = 1;
        c.gridx=1;
        c.gridy=2;
        panel.add(messages,c);
    }

    private ImageIcon resizeIcon(ImageIcon icon, JLabel label) {
        Image image = icon.getImage();
        Image newImg = image.getScaledInstance(55, 70, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(newImg);
    }
}
