import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.TimerTask;
import java.util.Timer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class mpServer {
    private static BlockingQueue<String> queue = new LinkedBlockingQueue<String>();
    static JFrame frame;
    private JTextArea outputArea;
    private ArrayList<ServerConnection> connections;
    private ServerSocket server;

    final int MAXPLAYERS = 3;

    Timer timer;
    Cards cards;
    Card card;
    int currentPlayer;
    int totalPlayers;
    int[] handValue;
    int[] splitHandValue;
    int[] bet;
    int totalBets;
    Boolean[] hasBust;
    Boolean finished = false;
    Boolean threadActive = true;
    Boolean[] hasAce;
    Boolean dealerHasAce = false;
    Boolean dealerHasHadAce;
    Boolean[] hasHadAce;
    Boolean dealerFinishedOnStart = false;  //Dealer got desired cards at start
    Boolean[] hasSplit;
    private Server chat;

    String[] playerName;


    public  mpServer(){
        cards = new Cards();

        frame = new JFrame("MultiPlayer Server");
        outputArea = new JTextArea();
        frame. setSize(500, 500);
        frame.setVisible(true);
        frame.add(outputArea, BorderLayout.CENTER);
        display("Server awaiting connections...");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        connections = new ArrayList<ServerConnection>();

        try {
            server = new ServerSocket(5500, 4);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }


    void playerMove(String move){
        if(move.length()>3 && move.startsWith("EXIT"))
            clientExit(Integer.parseInt(move.substring(4,5)));
        else {
            if (move.equals("HIT")) {
                card = cards.drawCard();
                handValue[currentPlayer] += card.value;
                if (card.value == 11)
                    hasAce[currentPlayer] = true;
                convertAce(card);
                sendToAll(new PlayObject(playerName[currentPlayer], card, currentPlayer, handValue[currentPlayer], bet[currentPlayer], "HIT"));
                System.out.println("Player: " + currentPlayer + " chose: HIT");
            } else if (move.equals("HIT1")){
                card = cards.drawCard();
                splitHandValue[currentPlayer] += card.value;
                sendToAll(new PlayObject(playerName[currentPlayer], card, currentPlayer, splitHandValue[currentPlayer], bet[currentPlayer], "HIT1"));
                System.out.println("Player: " + currentPlayer + " chose: HIT1");
            } else if (move.equals("STAND")) {
                System.out.println("Player: " + currentPlayer + " chose: STAND");
                if(hasSplit[currentPlayer])
                    hasSplit[currentPlayer] = false;
                else
                    incrementPlayer();

            } else if (move.equals("DOUBLE")) {
                card = cards.drawCard();
                handValue[currentPlayer] += card.value;
                bet[currentPlayer] *= 2;
                convertAce(card);
                sendToAll(new PlayObject(playerName[currentPlayer], card, currentPlayer, handValue[currentPlayer], bet[currentPlayer], "DOUBLE"));
                System.out.println("Player: " + currentPlayer + " chose: DOUBLE"); //Hit + double bet
            } else if (move.equals("SPLIT")) {
                System.out.println("Player: " + currentPlayer + " chose: SPLIT");
                sendToAll(new PlayObject(playerName[currentPlayer],null,currentPlayer,0, bet[currentPlayer], "SPLIT"));
                splitHandValue[currentPlayer] = handValue[currentPlayer]/2;  //Distributes score
                handValue[currentPlayer] = handValue[currentPlayer]/2;
                hasSplit[currentPlayer] = true;
            } else if (move.substring(0, 4).equals("NAME")) {
                playerName[Integer.parseInt(move.substring(4, 5))] = move.substring(5, move.length());
                System.out.println("ID: " + move.substring(4, 5));
                System.out.println("NAME: " + move.substring(5, move.length()));
            } else if (move.substring(0, 3).equals("BET")) {
                bet[Integer.parseInt(move.substring(3, 4))] = Integer.parseInt(move.substring(4, move.length()));
                totalBets++;
                if (totalBets == totalPlayers)
                    initialCards();
            }

            if (handValue[currentPlayer] > 21)
                hasBust[currentPlayer] = true;


            if (currentPlayer == 0) { //Dealer Play
                System.out.println("Dealer's turn");
                if (dealerFinishedOnStart) {
                    sendToAll(new PlayObject("Dealer", null, 0, handValue[0], 0, "FLIP"));  //Message to flip hidden card
                    gameFinished();
                } else
                    dealerAI();
            }
        }
    }

    private void convertAce(Card card){
        if(card.value == 11)
            hasAce[currentPlayer] = true;
        if(handValue[currentPlayer]>21 && hasAce[currentPlayer] && !hasHadAce[currentPlayer]) {  //Only converted once
            handValue[currentPlayer] -= 10;
            hasAce[currentPlayer] = false;
            hasHadAce[currentPlayer] = true;
        }
    }

    synchronized void dealerAI(){  //Recursive
        if(handValue[0] < 17) {
            delay(1500);
            card = cards.drawCard();
            if(card.value == 11)
                dealerHasAce = true;
            handValue[0] += card.value;

        }
        else if(dealerHasAce && handValue[0] == 17){
            delay(1500);
            card = cards.drawCard();
            handValue[0] += card.value;
        }
        else{
            card=null;
        }

        if(handValue[0]>21 && dealerHasAce && !dealerHasHadAce){  //Ace is only converted once
            handValue[0] -= 10;    //Ace goes from 11->1 if overshoots
            dealerHasAce = false;
            dealerHasHadAce = true; //Removes the chance to convert again
        }

        if(card!=null) {  //If dealer has not chosen to stand
            sendToAll(new PlayObject("DEALER" ,card, 0, handValue[0], 0, "HIT"));
        }

        if(handValue[0]<17 || (handValue[0]==17 && dealerHasAce))
            dealerAI();
        else
            gameFinished();
        System.out.println("Dealer sum: " + handValue[0]);
    }

    private void delay(int msec){
        try {
            Thread.sleep(msec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void gameFinished(){
        finished = true;

        for (int i = 1; i <= totalPlayers; i++) { //Dealer has not bust, player more || dealer has bust && player less than 22 ||
            if ( ((handValue[i] > handValue[0] && handValue[i]<22) || (handValue[0] > 21 && handValue[i]<22)) || (splitHandValue[i]!=0 && ((splitHandValue[i] > handValue[0] && splitHandValue[i]<22) || (handValue[0] > 21 && splitHandValue[i]<22))) )
                sendToAll(new PlayObject(playerName[i] ,null, i, handValue[i], bet[i], "WIN"));
            else if ((handValue[i] == handValue[0] && handValue[i]<22) || (splitHandValue[i] == handValue[0] && splitHandValue[i]<22))
                sendToAll(new PlayObject(playerName[i] ,null, i, handValue[i], bet[i], "EQUAL"));
            else
                sendToAll(new PlayObject(playerName[i] ,null, i, handValue[i], bet[i], "LOSE"));
        }

        newGame();
    }

    private void newGame(){
        delay(4000);  //Time to see cards

        sendToAll(new PlayObject(null,null,0,0,0,"NEWGAME"));
        dealerFinishedOnStart = false;
        finished = false;
        startGame();
    }


    private void incrementPlayer(){
        currentPlayer++;
        currentPlayer = currentPlayer%(totalPlayers+1); //For example, three players: 1, 2, 3, 0(DLR)
        if(currentPlayer!=0)
            connections.get(currentPlayer-1).clientsTurn(); //Ping the next players client telling its their turn
    }

    public void clientExit(int pNo){  //Keeps the integrity of the game when user exits.
        System.out.println("DELETE PLAYER: " +pNo);
        display(playerName[pNo] + " exited the game");
        sendToAll(new PlayObject(playerName[pNo] ,null,pNo,0,0,"DEL")); //Sends delete message
        //ServerConnection[] buffer = new ServerConnection[totalPlayers]; //Starts at 1 so actually (totalPlayers-1) spots

        for(int i = pNo; i<totalPlayers;i++){ //Moves all the users above one step down client side.
            sendToAll(new PlayObject(playerName[i] ,null,i+1,0,i,"UPDATE"));
            System.out.println("Player: " + (i+1) + ". Moved to : " + i);
        }

        connections.get(pNo-1).shouldRun = false;
        connections.remove(pNo-1);
        if(connections.size()>0) {
            totalPlayers--;         //One less player

            for (int i = 1; i <= totalPlayers; i++) {  //Refresh ID's so they are in order
                connections.get(i - 1).assignID(i);
                System.out.println("Sent ID: " + i + " to connection " + (i - 1));
            }


            if (currentPlayer == (totalPlayers + 1)) //Was the last player
            {
                currentPlayer = 0; //Dealers turn
                playerMove("");    //Resumes and goes straight to dealer code
            } else if (currentPlayer > pNo) { //If currentplayer is past leaving player
                currentPlayer--;                       //Goes back as the ID's has been pushed back
                connections.get(currentPlayer - 1).clientsTurn();      //The same player is still playing
            } else if (currentPlayer == pNo)
                connections.get(currentPlayer - 1).clientsTurn();  //Signal player who took his place
        }
        else
            restartServer();
    }

    public Card[] getStartingCards() {
        Card[] starterCards = new Card[2];
        starterCards[0] = cards.drawCard();
        starterCards[1] = cards.drawCard();
        return starterCards;
    }

    synchronized void execute(int noOfP){
        timer = new Timer();
        totalPlayers = 0;
        new Thread(()-> {
            consumer();
        }).start();

        Boolean isStarted = false;
        for(int i = noOfP;i<MAXPLAYERS;i++){
            try {
                outputArea.append("Trying to connect to: " + i+"\n");
                Socket s = server.accept();
                ServerConnection sc = new ServerConnection(s,i+1);
                connections.add(sc);
                display("server connected to connection number " + connections.get(i).playerNumber);
                if(!isStarted) {
                    timer.schedule(new App(), 0, 1000);
                    isStarted = true;
                }
                totalPlayers++;
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }


    //Starts the game
    public void startGame() {
        totalBets = 0;
        currentPlayer = 1;
        dealerHasHadAce = false;
        finished = false;
        hasHadAce = new Boolean[3+1];
        handValue = new int[3+1];
        splitHandValue = new int[3+1];
        bet = new int[3+1];
        hasBust = new Boolean[3+1];
        hasAce = new Boolean[3+1];
        playerName = new String[3+1];
        hasSplit = new Boolean[3+1];
        for(int i = 0; i<=3; i++){
            handValue[i] = 0;
            splitHandValue[i] = 0;
            bet[i] = 0;
            hasBust[i] = false;
            hasAce[i] = false;
            playerName[i] = "";
            hasSplit[i] = false;
            hasHadAce[i] = false;
        }

        System.out.println("Starting Game");
        //Assign ID to the client(s)
        for(int i = 0; i<totalPlayers;i++){
            connections.get(i).assignID(i+1);
        }

        sendToAll(new PlayObject(null,null,0,0,0,"START"));
    }

    public void initialCards(){
        //Give all players (and dealers) starting cards, and applies game rules to recieved cards
        for(int i = 1; i<=totalPlayers;i++){
            if(connections!=null) {
                Card[] startCards = getStartingCards();
                handValue[i] += startCards[0].value;
                sendToAll(new PlayObject(playerName[i] ,startCards[0],i, handValue[i], bet[i], "CARD"));
                handValue[i] += startCards[1].value;
                if(startCards[0].value == 11 && startCards[1].value == 11) {  //Double ace is immediately converted
                    handValue[i] = 12;
                    hasHadAce[i] = true;
                }
                sendToAll(new PlayObject(playerName[i] ,startCards[1],i, handValue[i], bet[i], "CARD"));
                if((startCards[0].value == 11 || startCards[1].value == 11) && !hasHadAce[i])
                    hasAce[i] = true;
            }
        }

        Card[] dealerCards = getStartingCards();
        handValue[0] += dealerCards[0].value;
        sendToAll(new PlayObject("Dealer" ,dealerCards[0],0,handValue[0],0, "CARD")); //Dealer is "0"
        handValue[0] += dealerCards[1].value;
        if(dealerCards[0].value == 11 && dealerCards[1].value == 11) {  //Double ace is immediately converted
            handValue[0] = 12;
            dealerHasHadAce = true;
        }
        sendToAll(new PlayObject("Dealer" ,dealerCards[1],0,handValue[0],0, "CARD")); //Dealer has no bet

        if((dealerCards[0].value == 11 || dealerCards[1].value == 11) && !dealerHasHadAce)
            dealerHasAce = true;

        if((handValue[0]==17 && !dealerHasAce) || (handValue[0]>17))
            dealerFinishedOnStart = true;

        System.out.println("Sent all ID's");
        connections.get(currentPlayer-1).clientsTurn();  //Start by making it the first players turn
    }

    public void sendToAll(PlayObject obj) {
        for(int i = 0; i<connections.size();i++) {
            if(!((obj.getID()==i) && ((obj.getPurpose().equals("DEL") || (obj.getPurpose().equals("UPDATE")))))){
                connections.get(i).sendPlayObj(obj);   //Is not the deleted player
            }
        }

    }


    public void display(String text){    //displays text to the server
        outputArea.append(text + "\n");
    }

    public void consumer(){
        String myObject = null;
        while(threadActive){
            try {
                myObject = queue.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            playerMove(myObject);
        }
    }

    static public void main(String[] args){
        mpServer mp= new mpServer();
        Server chat = new Server();
        mp.execute(0);
        chat.execute(0);
    }

    public void restartServer(){
        //chat.restart();
        connections = new ArrayList<ServerConnection>();
        threadActive = false;
        try {
            server.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        delay(1000);
        try {
            server = new ServerSocket(5500, 4);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        connections.clear();
        outputArea.append("Restarted Server!\n");
        threadActive = true;
        execute(0);
    }

    private class ServerConnection extends Thread{
        private Socket connection;

        //Reads string containing client's actions
        private DataInputStream input;

        //stream channel for sending play-objects to the client and strings
        private ObjectOutputStream output;

        public int playerNumber;
        boolean shouldRun = true;

        public ServerConnection(Socket socket, int number){
            connection = socket;
            playerNumber = number;

            try {
                input = new DataInputStream(connection.getInputStream());
                output = new ObjectOutputStream(connection.getOutputStream());
                output.flush();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            start();
        }

        public void run(){
            display("player "+ playerNumber + " connected");
            try{

                while(shouldRun){
                    //Read Command from client:
                    String textIn = input.readUTF();
                    System.out.println("Player wrote: " + textIn);
                    if(textIn.equals("EXIT")) {
                        input.close();
                        output.close();
                        connection.close();
                        queue.put(textIn+playerNumber);
                    }
                    else{
                        if(!finished) {
                            queue.put(textIn); // Blocks until queue isn't full.
                        }

                    }
                }

                input.close();
                output.close();
                connection.close();
            }
            catch(IOException | InterruptedException e){
                e.printStackTrace();
            }
        }

        public void clientsTurn(){
            try {
                output.writeObject(new PlayObject("Username" ,null,currentPlayer,0,0,"TURN"));
                output.flush();
                System.out.println("Sent TURN Message");
            } catch (IOException ioException){
                ioException.printStackTrace();
            }
        }

        public void assignID(int i) {
            try {
                output.writeObject(new PlayObject("Username" ,null,i,0,0,"ID"));
                output.flush();
                playerNumber = i;
                System.out.println("Sent ID: "+i);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //Sends a object containing a new card (or null if player chose to stand)
        //and which player-number it is intended for
        public void sendPlayObj(PlayObject pObj){
            try {
                output.writeObject(pObj);
                output.flush();

                System.out.println("Sent: "+ pObj.getPurpose());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    //For the timer:
    class App extends TimerTask {

        int countdown = 15;

        public void run() {
            display("game starting in " + countdown);
            countdown = countdown - 1;
            System.out.println(countdown);
            if(countdown==0){
                System.out.println("Time's up!");
                display("game started!");
                timer.cancel();
                try {
                    server.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                startGame();
            }
        }

    }
}

