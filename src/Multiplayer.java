import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Locale;
import java.util.ResourceBundle;

public class Multiplayer {
    PlayerPanel[] otherPlayer = new PlayerPanel[3+1];
    PlayerPanel playerPanel;
    PlayerPanel dealerPanel;
    Client message;
    static JFrame frame;

    static JButton stand;
    static JButton hit;
    static JButton split;
    static JButton doubleDown;
    static JButton leaveGame;

    static JButton chip5;
    static JButton chip10;
    static JButton chip50;
    static JButton chip100;
    static JButton submitBet;

    static JLabel balanceLabel;
    static JLabel playerName;
    static JLabel messageLabel;

    static ResourceBundle game;
    static String lang = "en";
    static Boolean created = false;
    static Boolean cantDoubleDown;
    static Boolean shouldRun = true; //stops listening on the stream
    private Player user;

    private Card[] firstTwo;  //First two cards of the user
    private int amountOfCards;
    private boolean canSplit = false;
    Boolean splitActive = false;

    Font calibri;

    private Card hiddenCard;
    private boolean cardHidden;
    private boolean notConnected;

    private ObjectInputStream input;
    private DataOutputStream output;         //Flow in to your computer
    private String serverIP;
    private Socket connection;
    private PlayObject pObj;

    private int myID;        //This player's ID
    private Boolean bust;    //Has the player bust?

    //Resizes icon to fit label
    private static ImageIcon resizeIcon(ImageIcon icon, int Width, int Height) {
        Image img = icon.getImage();                                                   //Makes image object
        Image resizedIcon = img.getScaledInstance(Width, Height, java.awt.Image.SCALE_SMOOTH);  //Scales image
        return new ImageIcon(resizedIcon);                                                      //returns resized imageobject
    }

    Multiplayer(Player user) {
        //serverIP="192.168.0.117";
        serverIP="127.0.0.1";
        bust = false;
        cantDoubleDown = false;
        this.user = user;
        game = ResourceBundle.getBundle("game", Locale.forLanguageTag(lang));
        calibri = new Font(Font.DIALOG_INPUT, Font.BOLD, 18);     //Declares new font

        frame = new JFrame(game.getString("titleMP"));
        ImageIcon logoIcon = new ImageIcon(this.getClass().getResource("Resources/defaultIcon.png"));
        Image img = logoIcon.getImage();
        frame.setIconImage(img);

        amountOfCards = 0;
        firstTwo = new Card[2];

        //Chat:
        message = new Client("127.0.0.1", user);

        notConnected = true;
        new Thread(()-> {
            initiateSockets();
        }).start();

        frame.setSize(1200, 650);
        frame.getContentPane().setBackground(Color.DARK_GRAY);
        frame.setJMenuBar(Menubar.Menubar(frame, lang, user.getUserName()));

        //Panel for user of GUI
        playerPanel = new PlayerPanel(user.getUserName());
        playerPanel.getPanel().setBounds(390,360,225,220);
        frame.add(playerPanel.panel);

        //DealerPanel
        dealerPanel = new PlayerPanel(game.getString("dealer"));
        dealerPanel.getPanel().setBounds(410,10,225,160);
        frame.add(dealerPanel.panel);

        messageLabel = new JLabel("WAIT FOR CONNECTION",SwingConstants.CENTER);
        balanceLabel = new JLabel("Balance: " + user.getBalance());
        playerName = new JLabel("Welcome " + user.getUserName()+"!");
        playerName.setBounds(980,520,250,50);
        balanceLabel.setBounds(980,540,250,50);
        messageLabel.setBounds(365,310,250,50);
        messageLabel.setForeground(Color.white);
        playerName.setForeground(Color.white);
        balanceLabel.setForeground(Color.white);
        messageLabel.setFont(calibri);
        playerName.setFont(calibri);
        balanceLabel.setFont(calibri);
        frame.add(messageLabel);
        frame.add(balanceLabel);
        frame.add(playerName);

        frame.setResizable(false);

        setupOtherPlayers();

        setupButtons();

        //Background
        ImageIcon background = new ImageIcon(this.getClass().getResource("Resources/background.png"));
        JLabel contentPane = new JLabel();
        contentPane.setBounds(40, 0, 900, 600);
        contentPane.setIcon(resizeIcon(background, contentPane.getWidth(), contentPane.getHeight()));

        frame.getContentPane().add(contentPane);


        //Chat Panel:
        frame.add(message.getPanel());
        message.getPanel().setOpaque(false);



        frame.setVisible(true);
        frame.setLayout(null);
        created = true;

        frame.addWindowListener(new WindowAdapter(){  //Makes the user confirm exit and notifies server
            public void windowClosing(WindowEvent e){
                if(!notConnected) {
                    sendCommand("EXIT");
                    message.stopRunning();
                }
                notConnected = false;  //Stops trying to connect
                shouldRun = false;
                DBConnect db = new DBConnect();
                db.removeUserFromLoggedInTable(user.getUserName());   //Logs out user upon exit
                user.changeBalance(user.getBalance());
                frame.dispose();
            }
        });
    }


    private void newGame(){
        cantDoubleDown = false;
        canSplit = false;
        splitActive = false;
        amountOfCards = 0;
        firstTwo[0] = null;   //Reset starter cards
        firstTwo[1] = null;
        for (int i = 1; i <= 3; i++) {
            otherPlayer[i].reset();
        }
        playerPanel.reset();
        dealerPanel.reset();
        hiddenCard = null;
        cardHidden = false;
        split.setEnabled(false);
    }

    public void initiateSockets(){   //Retries connection, runs object listener when successful.
        try {
            while(notConnected) {
                try {
                    connection = new Socket(InetAddress.getByName(serverIP), 5500);
                    input = new ObjectInputStream(connection.getInputStream());
                    output = new DataOutputStream(connection.getOutputStream());
                    output.flush();
                    notConnected = false;
                }
                catch(java.net.ConnectException e) {  //check if connection was not successful
                    notConnected = true;
                }

            }
            if(!notConnected){
                new Thread(() -> {
                    message.startRunning();
                }).start();

                new Thread(()-> {
                    objListener();
                }).start();
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    public void objListener(){
        while(shouldRun) {
            try {
                Object object = (PlayObject) input.readObject();  //Reads object
                if(object != null) {
                    pObj = (PlayObject) object; //Casts object into correct form
                    System.out.println(pObj.purpose);
                    receivedObject(pObj);
                }

            } catch (IOException | ClassNotFoundException ioException) {
                //ioException.printStackTrace();
            }
        }
        try {
            input.close();
            output.close();
            connection.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    private void receivedObject(PlayObject obj) {
        if(obj.getPurpose().equals("ID")){
            myID = obj.getID();
            sendCommand("NAME"+myID+user.getUserName());   //Sends username upon getting the ID
        }
        else if(obj.getPurpose().equals("CARD")){
            common(obj,0);
            if(obj.getID()==myID){
                //Split logic:
                if(amountOfCards==0){
                    firstTwo[0] = obj.getCard();
                    amountOfCards++;
                }
                else if(amountOfCards==1){
                    firstTwo[1] = obj.getCard();
                    amountOfCards++;
                }
                if((amountOfCards==2) && (firstTwo[0].value == firstTwo[1].value)){
                    canSplit = true;
                    amountOfCards++; //Just for safe measure
                }
                if(obj.getHand() == 21)
                {
                    playerPanel.changeMessage("BLACKJACK");
                    //toggleButtons(false);      //This did not run for some reason
                    //sendCommand("STAND");
                }
            }
        }
        else if(obj.getPurpose().equals("TURN")){
            toggleButtons(true);
            playerPanel.changeMessage("Your Turn!");
            messageLabel.setText("YOUR TURN");
            if(canSplit)
                split.setEnabled(true);
        }
        else if(obj.getPurpose().equals("HIT")){
            common(obj,0);
        }
        else if(obj.getPurpose().equals("HIT1")){
            common(obj,1);
        }
        else if(obj.getPurpose().equals("STAND")){
            messageLabel.setText(obj.getUsername()+" CHOSE STAND");
        }
        else if(obj.getPurpose().equals("DOUBLE")){
            common(obj,0);
            if(obj.getID() == myID) {
                user.setBalance(user.getBalance() - playerPanel.getBet());
                balanceLabel.setText("Balance: "+user.getBalance());
                sendCommand("STAND");
            }
        }
        else if(obj.getPurpose().equals("SPLIT")) {
            if (obj.getID() != myID){
                otherPlayer[obj.getID()].doSplit();
            }
        }
        else if(obj.getPurpose().equals("DEL")){  //Removes gone players
            otherPlayer[obj.getID()].reset();
            messageLabel.setText(obj.getUsername()+" LEFT THE GAME");
        }
        else if(obj.getPurpose().equals("UPDATE")){  //Moves a player panel when someone leaves
            otherPlayer[obj.getBet()].copyCards(otherPlayer[obj.getID()]);
            otherPlayer[obj.getID()].reset();
        }
        else if(obj.getPurpose().equals("WIN")){
            int winAmount=0;
            if(obj.getID()==myID) {
                playerPanel.changeMessage("YOU WIN");
                if(playerPanel.usedCards==2 && obj.getHand()==21) {
                    winAmount = obj.getBet();
                    winAmount = (winAmount * 2 + winAmount/2);
                    playerPanel.changeMessage("BLACKJACK, YOU WIN");
                } else {
                    winAmount = obj.getBet() * 2;
                }
                user.changeBalance(user.getBalance()+winAmount); //Pays 3 to 2!
                balanceLabel.setText("Balance: "+user.getBalance());
                messageLabel.setText("YOU WIN "+winAmount+"$");
            }
            else
                otherPlayer[obj.getID()].changeMessage("WIN");
        }
        else if(obj.getPurpose().equals("EQUAL")){
            if(obj.getID()==myID) {
                playerPanel.changeMessage("Tie!");
                messageLabel.setText("TIE, GOT " + playerPanel.getBet() + " BACK");
                user.changeBalance(user.getBalance() + playerPanel.getBet()); //restores balance
                balanceLabel.setText("Balance: " + user.getBalance());
            }
            else
                otherPlayer[obj.getID()].changeMessage("Equal to dealer");
        }
        else if(obj.getPurpose().equals("LOSE")){
            if(obj.getID()==myID) {
                playerPanel.changeMessage("YOU LOSE");
                messageLabel.setText("YOU LOSE");
            }
            else
                otherPlayer[obj.getID()].changeMessage("LOSE");
        }
        else if(obj.getPurpose().equals("NEWGAME")){
            messageLabel.setText("GET READY FOR A NEW ROUND");
            newGame();
        }
        else if(obj.getPurpose().equals("START")){
            showBets(true);
            submitBet.setEnabled(false);  //Disable button until bet is put
            messageLabel.setText("START BETTING");
            if(user.getBalance()<5){  //Kicks the player out if balance is lower than 5
                sendCommand("EXIT");
                shouldRun = false;
                user.changeBalance(user.getBalance());
                frame.dispose();
                Home mainscreen = new Home();
                mainscreen.RunHome(user);
            }
        }
        else if(obj.getPurpose().equals("FLIP")){
            dealerPanel.addCard(hiddenCard);
            cardHidden = false;
            hiddenCard = null;
        }

    }

    private void common(PlayObject obj, int hand) {
        if(obj.getID() == 0){
            if(dealerPanel.usedCards!=1) {
                dealerPanel.addCard(obj.getCard());
                dealerPanel.changeHandValue(obj.getHand());
            }
            else {
                if(!cardHidden) {
                    dealerPanel.setBackside();
                    hiddenCard = obj.getCard();
                    cardHidden = true;
                }
                else{
                    cardHidden = false;
                    dealerPanel.addCard(hiddenCard);
                    dealerPanel.addCard(obj.getCard());
                    dealerPanel.changeHandValue(obj.getHand());
                }
            }
        }
        else if (obj.getID() == myID){
            if(hand==1){
                playerPanel.addSplitCard(obj.getCard());
                playerPanel.changeHandValueSplit(obj.getHand());
                playerPanel.setBet(obj.getBet());
                if (obj.getHand() > 21) {
                    playerPanel.changeMessage("BUST FIRST HAND");
                    playerPanel.createBorder("RESET");
                    playerPanel.createBorder("FIRSTHAND");
                    splitActive = false;   //returns to hitting on first hand
                    if(!obj.getPurpose().equals("DOUBLE"))
                        sendCommand("STAND");
                }
            }
            else {
                playerPanel.addCard(obj.getCard());
                playerPanel.changeHandValue(obj.getHand());
                playerPanel.setBet(obj.getBet());
                if (obj.getHand() > 21) {
                    playerPanel.changeMessage("BUST");
                    playerPanel.createBorder("RESET");
                    toggleButtons(false);
                    if(!obj.getPurpose().equals("DOUBLE"))
                        sendCommand("STAND");
                }
            }
        }
        else {
            if(hand==1){
                otherPlayer[obj.getID()].addSplitCard(obj.getCard());
                otherPlayer[obj.getID()].changeHandValueSplit(obj.getHand());
            }
            else {
                otherPlayer[obj.getID()].addCard(obj.getCard());
                otherPlayer[obj.getID()].changeHandValue(obj.getHand());
            }
            otherPlayer[obj.getID()].setBet(obj.getBet());
            otherPlayer[obj.getID()].changeName(obj.getUsername());
        }
        if(!obj.getPurpose().equals("CARD"))
            messageLabel.setText(obj.getUsername()+" CHOSE "+obj.getPurpose());
    }

    private void sendCommand(String command){
        try{
            output.writeUTF(command);     //Sends message
            System.out.println("Sent: " + command);
            output.flush();                              //Empties buffers and sends header information

        }catch (IOException ioException){
            System.out.println(ioException);
        }
    }

    private void setupOtherPlayers(){
        for(int i=1;i<=3;i++){
            otherPlayer[i] = new PlayerPanel(game.getString("empty"));
            frame.add(otherPlayer[i].getPanel());
        }
        otherPlayer[1].getPanel().setBounds(100,100,225,180);
        otherPlayer[2].getPanel().setBounds(150,320,225,180);
        otherPlayer[3].getPanel().setBounds(700,100,225,180);
    }

    static void updateGUI(String s) {
        game = ResourceBundle.getBundle("game", Locale.forLanguageTag(s));
        frame.setTitle(game.getString("titleMP"));
        hit.setText(game.getString("hit"));
        split.setText(game.getString("split"));
        doubleDown.setText(game.getString("double"));
        leaveGame.setText(game.getString("leaveGame"));
    }


    private void setupButtons() {
        stand = new JButton("Stand");
        hit = new JButton(game.getString("hit"));
        split = new JButton(game.getString("split"));
        doubleDown = new JButton(game.getString("double"));
        leaveGame = new JButton(game.getString("leaveGame"));

        leaveGame.setBounds(  10,530, 140,50);

        stand.setBounds(     810,350,160,50);
        hit.setBounds(       810,410,160,50);
        doubleDown.setBounds(810,470,160,50);
        split.setBounds(     810,530,160,50);

        split.setEnabled(false); //Remove when functionality is present

        frame.add(stand);
        frame.add(hit);
        frame.add(split);
        frame.add(doubleDown);
        frame.add(leaveGame);

        stand.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playerPanel.createBorder("RESET");
                if(!splitActive) {
                    playerPanel.changeMessage("");
                    toggleButtons(false);
                    messageLabel.setText("");
                }
                else {
                    splitActive = false;
                    playerPanel.createBorder("FIRSTHAND");
                }

                sendCommand("STAND");
            }
        });

        hit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doubleDown.setEnabled(false);

                if (canSplit) {       //Did not choose to split
                    canSplit = false;
                    split.setEnabled(false);
                }

                if(splitActive)  //Begins hitting on first hand
                    sendCommand("HIT1");
                else
                    sendCommand("HIT");

            }
        });

        doubleDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand("DOUBLE");
                if (canSplit) {        //Did not choose to split
                    canSplit = false;
                    split.setEnabled(false);
                }
                toggleButtons(false);
            }
        });
        split.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendCommand("SPLIT");
                if (canSplit) {       //Chose split
                    canSplit = false;
                    split.setEnabled(false);
                }
                playerPanel.doSplit();
                splitActive = true;
                doubleDown.setEnabled(false);
                playerPanel.createBorder("SPLIT");
            }
        });
        leaveGame.addActionListener(new ActionListener() {           //If leavegame is clicked
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!notConnected){
                    sendCommand("EXIT");
                    message.stopRunning();
                }
                //shouldRun = false;
                //message.stopRunning();
                notConnected = false;
                frame.dispose();
                user.changeBalance(user.getBalance());  //Updates balance to the database
                Home mainscreen = new Home();
                mainscreen.RunHome(user);
            }
        });

        toggleButtons(false);

        chip5=new JButton();                                    //Declares clickable chips
        chip10=new JButton();
        chip50=new JButton();
        chip100=new JButton();
        submitBet = new JButton("Submit");
        chip5.setIcon(playerPanel.tokens[0]);
        chip10.setIcon(playerPanel.tokens[1]);
        chip50.setIcon(playerPanel.tokens[2]);
        chip100.setIcon(playerPanel.tokens[3]);
        chip5.setBounds(400,390,50,50);
        chip10.setBounds(450,390,50,50);
        chip50.setBounds(490,390,50,50);
        chip100.setBounds(530,390,50,50);
        submitBet.setBounds(450,350,80,30);
        chip5.setBorder(null);
        chip10.setBorder(null);
        chip50.setBorder(null);
        chip100.setBorder(null);
        chip5.setContentAreaFilled(false);
        chip10.setContentAreaFilled(false);
        chip50.setContentAreaFilled(false);
        chip100.setContentAreaFilled(false);

        showBets(false);

        frame.add(chip5);
        frame.add(chip10);
        frame.add(chip50);
        frame.add(chip100);
        frame.add(submitBet);

        betAction();  //Set up bet action listeners

    }

    private void betAction() {                                       //Adds bet if chips is clicked
        //info.setText("Place bets!");
        chip5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if((user.getBalance() - playerPanel.getBet()) >=5) {
                    playerPanel.addBet(5);
                    submitBet.setEnabled(true);
                }
            }
        });
        chip10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if((user.getBalance() - playerPanel.getBet()) >=10) {
                    playerPanel.addBet(10);
                    submitBet.setEnabled(true);
                }
            }
        });
        chip50.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if((user.getBalance() - playerPanel.getBet()) >=50) {
                    playerPanel.addBet(50);
                    submitBet.setEnabled(true);
                }
            }
        });
        chip100.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if((user.getBalance() - playerPanel.getBet()) >=100) {
                    playerPanel.addBet(100);
                    submitBet.setEnabled(true);
                }
            }
        });
        submitBet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showBets(false);
                sendCommand("BET"+myID+playerPanel.getBet());
                user.setBalance(user.getBalance()-playerPanel.getBet());
                balanceLabel.setText("Balance: "+user.getBalance());
                messageLabel.setText("");
                if(playerPanel.getBet()>user.getBalance())  //Can't double down if not enough money
                    cantDoubleDown = true;
            }
        });

    }

    private void toggleButtons(boolean state) {
        stand.setEnabled(state);
        hit.setEnabled(state);
        doubleDown.setEnabled(state);
        if(cantDoubleDown)
            doubleDown.setEnabled(false);
    }

    private void showBets(boolean state){                  //Show chips
        chip5.setVisible(state);
        chip10.setVisible(state);
        chip50.setVisible(state);
        chip100.setVisible(state);
        submitBet.setVisible(state);
    }

    public static JFrame getFrame(){
        return frame;
    }

}

