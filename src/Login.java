
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.MenuKeyEvent;
import javax.swing.event.MenuKeyListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadLocalRandom;

public class Login {
    static JFrame frame;
    private JTextField textField;
    private JPasswordField passwordField;
    static JButton loginButton;
    static JButton guestButton;
    static JButton newUserButton;
    private JLabel label = new JLabel();
    static JLabel lblPassword;
    static JLabel lblUsername;
    static JLabel lblNewLabel;
    private JPanel loginPanel;
    private int startBalance = 10000;
    Home mainscreen = new Home();
    static String lang = "en";
    static ResourceBundle login;


    public void Login() {
        login = ResourceBundle.getBundle("login", Locale.forLanguageTag(lang));
        ImageIcon backGroundimg = new ImageIcon(this.getClass().getResource("Resources/background_green.jpg"));
        frame = new JFrame(login.getString("title") + ": Blackjack");
        frame.setSize(700, 597);

        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setJMenuBar(Menubar.Menubar(frame, lang, null));  //Adding the menubar

        ImageIcon logoIcon = new ImageIcon(this.getClass().getResource("Resources/defaultIcon.png"));
        Image img = logoIcon.getImage();
        frame.setIconImage(img);


        JLayeredPane Layeredpane = new JLayeredPane();             //Make a new layeredpane

        JLabel backGround = new JLabel();
        backGround.setBounds(0, 0, 700, 597);
        backGround.setIcon(backGroundimg);
        Layeredpane.add(backGround, JLayeredPane.DEFAULT_LAYER);   //Add background behind the components


        lblNewLabel = new JLabel(login.getString("title"));      //Title
        lblNewLabel.setForeground(Color.LIGHT_GRAY);
        lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 46));
        lblNewLabel.setBounds(266, 13, 273, 93);
        Layeredpane.add(lblNewLabel, JLayeredPane.POPUP_LAYER);


        label = new JLabel("");                  //Label used to give login messages
        label.setBounds(93,90,300,50);
        label.setForeground(Color.lightGray);
        label.setBackground(Color.lightGray);

        Layeredpane.add(label, JLayeredPane.POPUP_LAYER);

        lblUsername = new JLabel(login.getString("username"));            //Username label
        lblUsername.setBackground(Color.LIGHT_GRAY);
        lblUsername.setForeground(Color.LIGHT_GRAY);
        lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 31));
        lblUsername.setBounds(93, 130, 193, 52);
        Layeredpane.add(lblUsername, JLayeredPane.POPUP_LAYER);

        textField = new JTextField();                //Textfield to insert the username
        textField.setFont(new Font("Tahoma", Font.PLAIN, 32));
        textField.setBounds(324, 134, 281, 68);
        Layeredpane.add(textField, JLayeredPane.POPUP_LAYER);


        lblPassword = new JLabel(login.getString("password"));           //Password label
        lblPassword.setForeground(Color.LIGHT_GRAY);
        lblPassword.setBackground(Color.LIGHT_GRAY);
        lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 31));
        lblPassword.setBounds(93, 200, 193, 52);
        Layeredpane.add(lblPassword, JLayeredPane.POPUP_LAYER);


        passwordField = new JPasswordField();      //Textfield to insert the password
        passwordField.setFont(new Font("Tahoma", Font.PLAIN, 32));
        passwordField.setBounds(324, 204, 281, 68);
        Layeredpane.add(passwordField, JLayeredPane.POPUP_LAYER);


        loginButton = new JButton(login.getString("login"));   //Button to login with the username and password
        loginButton.setFont(new Font("Tahoma", Font.PLAIN, 26));
        loginButton.setBackground(Color.LIGHT_GRAY);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String userName = textField.getText();
                String password = passwordField.getText();
                loginWithUser(userName, password);

            }
        });
        loginButton.setBounds(388, 270, 162, 73);
        Layeredpane.add(loginButton, JLayeredPane.POPUP_LAYER);

        guestButton = new JButton(login.getString("guest"));   //Button to login as a guest user
        guestButton.setFont(new Font("Tahoma", Font.PLAIN, 26));
        guestButton.setBackground(Color.LIGHT_GRAY);
        guestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                String userName = createGuestName();      //Calling function to create a random guestname
                Player player = new Player(userName, null, startBalance);
                mainscreen.RunHome(player);    //Homescreen

            }
        });
        guestButton.setBounds(93,390, 281, 73);
        Layeredpane.add(guestButton, JLayeredPane.POPUP_LAYER);


        newUserButton = new JButton(login.getString("newUser"));      //Button to create a new user
        newUserButton.setFont(new Font("Tahoma", Font.PLAIN, 26));
        newUserButton.setBackground(Color.LIGHT_GRAY);
        newUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                CreateNewUser c = new CreateNewUser();
                c.CreateNewUser();
            }
        });
        newUserButton.setBounds(93, 470, 281, 73);
        Layeredpane.add(newUserButton, JLayeredPane.POPUP_LAYER);

        frame.add(Layeredpane);
        frame.setVisible(true);
    }




    static void updateGUI(String s) {
        login = ResourceBundle.getBundle("login", Locale.forLanguageTag(s));
        newUserButton.setText(login.getString("newUser"));
        guestButton.setText(login.getString("guest"));
        loginButton.setText(login.getString("login"));
        lblPassword.setText(login.getString("password"));
        lblUsername.setText(login.getString("username"));
        lblNewLabel.setText(login.getString("title"));
        frame.setTitle(login.getString("title") + ": Blackjack");
    }

    private void loginWithUser(String userName, String password) {
        DBConnect db = new DBConnect();
        int balance = db.getBalance(userName);
        if(db.logInDB(userName, password)){   //If username and password is correct
            if(!db.isUserLoggedIn(userName)){        //If the user is not already logged in
                frame.dispose();
                Player player = new Player(userName, password, balance);
                mainscreen.RunHome(player);   //Homescreen player object
                db.insertIntoLoggedInUsers(userName);
            }else{                                    //If the user is already logged in
                label.setText(login.getString("labelmsg2"));
            }
        }else {                                        //If the username or password is incorrect
            label.setText(login.getString("labelmsg1"));
        }
    }

    private String createGuestName(){    //Function that returns a random guest name
        String userName = "Guest";
        int randomNum = ThreadLocalRandom.current().nextInt(0, 10000);
        userName = userName + randomNum;
        return userName;
    }


}