import org.w3c.dom.html.HTMLObjectElement;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Locale;
import java.util.ResourceBundle;

public class Menubar {
    static JMenu file;
    static JMenu about;
    static JMenu help;
    static JMenu language;
    static ResourceBundle RBmenubar;
    static String lang = "en";
    static JMenuItem exit;
    static JMenuItem version;
    static JMenuItem authors;
    static JMenuItem howTo;
    static JMenuItem dictionary;
    static ImageIcon exitimg, noFlag, enFlag, question, dictionaryIcon;
    //mageIcon exitimg = new ImageIcon(this.getClass().getResource("Resources/exit.png"));
    static BufferedImage backgroundGreen;


    public void readTextfile(String path, String name) {
        JFrame f = new JFrame(name);
        f.setSize(300,300);
        JTextArea text = new JTextArea();
        text.setLineWrap(true);
        text.setWrapStyleWord(true);
        text.setBounds(10,10, 280, 280);
        text.setFont(new Font("Calibri", Font.PLAIN, 12));

        f.add(text);
        f.setVisible(true);
        try {
            InputStream is = getClass().getResourceAsStream(path);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            StringBuffer sb = new StringBuffer();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            text.setText(sb.toString());
            br.close();
            is.close();
            isr.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    static void updateGUI(String s) {
        RBmenubar = ResourceBundle.getBundle("home", Locale.forLanguageTag(s));
        file.setText(RBmenubar.getString("file"));
        about.setText(RBmenubar.getString("about"));
        help.setText(RBmenubar.getString("help"));
        language.setText(RBmenubar.getString("language"));
        exit.setText(RBmenubar.getString("exit"));
        version.setText(RBmenubar.getString("version"));
        authors.setText(RBmenubar.getString("authors"));
        howTo.setText(RBmenubar.getString("howToPlay"));
        dictionary.setText(RBmenubar.getString("dic"));
    }


    public static JMenuBar Menubar(JFrame f, String la, String userName){
        JMenuBar menu = new JMenuBar();
        RBmenubar = ResourceBundle.getBundle("home", Locale.forLanguageTag(la));

        exitimg = new ImageIcon(new ImageIcon(Menubar.class.getResource("Resources/exit.png")).getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
        noFlag = new ImageIcon(new ImageIcon(Menubar.class.getResource("Resources/norwegianFlag.png")).getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
        enFlag = new ImageIcon(new ImageIcon(Menubar.class.getResource("Resources/englishFlag.png")).getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
        question = new ImageIcon(new ImageIcon(Menubar.class.getResource("Resources/question.png")).getImage().getScaledInstance(15, 15, Image.SCALE_DEFAULT));
        dictionaryIcon = new ImageIcon(new ImageIcon(Menubar.class.getResource("Resources/dictionary.png")).getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));

        //java.net.URL imageURL = Menubar.class.getResource("Resources/BackgroundSP.png");
        //System.out.println(imageURL); //imageURL is printing correctly in console


        file = new JMenu(RBmenubar.getString("file"));
        about = new JMenu(RBmenubar.getString("about"));
        help = new JMenu(RBmenubar.getString("help"));
        language = new JMenu(RBmenubar.getString("language"));

        exit = new JMenuItem(RBmenubar.getString("exit"), exitimg);
        version = new JMenuItem(RBmenubar.getString("version"));
        authors = new JMenuItem(RBmenubar.getString("authors"));
        howTo = new JMenuItem(RBmenubar.getString("howToPlay"), question);
        dictionary = new JMenuItem(RBmenubar.getString("dic"), dictionaryIcon);
        JRadioButtonMenuItem english = new JRadioButtonMenuItem("English", enFlag);
        JRadioButtonMenuItem norwegian = new JRadioButtonMenuItem("Norwegian", noFlag);

        if(Login.lang == "no"){
            norwegian.setSelected(true);
        }
        else if (Login.lang == "en"){
            english.setSelected(true);
        }
        else{
            english.setSelected(true);
        }


        ButtonGroup languageGroup = new ButtonGroup();
        languageGroup.add(english);
        languageGroup.add(norwegian);




        file.add(exit);
        about.add(version);
        about.addSeparator();
        about.add(authors);
        help.add(howTo);
        help.addSeparator();
        help.add(dictionary);
        language.add(english);
        language.add(norwegian);


        menu.add(file);
        menu.add(about);
        menu.add(help);
        menu.add(language);

        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ans = JOptionPane.showConfirmDialog(null,
                        RBmenubar.getString("exitMsg"),RBmenubar.getString("exit"),
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                if(ans == 0){
                    f.dispose();
                    if(userName != null){
                        DBConnect db = new DBConnect();
                        db.removeUserFromLoggedInTable(userName);
                    }


                }
            }
        });

        version.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                version.setText("1.0.0");
            }
        });

        version.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) {
                version.setText(RBmenubar.getString("version"));
            }
        });

        authors.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,"Jesper Trøan\nNils Olav Tuv\n" +
                                                "Kristian Tveiten\nEmma Sofie Søvik\nJulian Skattum Nyland");
            }
        });

        howTo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResourceBundle r = ResourceBundle.getBundle("home", Locale.forLanguageTag(lang));
                JFrame f = new JFrame(r.getString("howToPlay"));
                f.setSize(500,300);

                try{
                    backgroundGreen = ImageIO.read((this.getClass().getResource("Resources/background_green.jpg")));  //The background for the frame
                }catch(IOException ex){
                    ex.printStackTrace();
                }

                JPanel howtoPanel = new JPanel(){
                    @Override
                    public void paintComponent(Graphics g)   //Drawing the background
                    {
                        g.drawImage(backgroundGreen,0,0,500,300, null); //Setting the background image as background in the panel
                    }
                };
                howtoPanel.setSize(500,300);

                JTextArea text = new JTextArea();
                text.setLineWrap(true);
                text.setWrapStyleWord(true);
                text.setOpaque(false);
                text.setForeground(Color.LIGHT_GRAY);
                text.setBounds(10,10, 400, 280);
                text.setFont(new Font("Calibri", Font.PLAIN, 14));

                howtoPanel.add(text);

                f.add(howtoPanel);
                f.setVisible(true);
                text.append(r.getString("howToPlay") + "\n\n");
                text.append(r.getString("H2Ptxt1") + "\n");
                text.append(r.getString("H2Ptitle1") + "\n\n");
                text.append(r.getString("H2Ptxt2") + "\n\n");
                text.append(r.getString("H2Ptitle2"));
                text.setEditable(false);
            }
        });

        dictionary.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResourceBundle r = ResourceBundle.getBundle("game", Locale.forLanguageTag(lang));
                JFrame f = new JFrame(r.getString("dic"));
                f.setSize(500,550);

                try{
                    backgroundGreen = ImageIO.read((this.getClass().getResource("Resources/background_green.jpg")));  //The background for the frame
                }catch(IOException ex){
                    ex.printStackTrace();
                }

                JPanel dictionaryPanel = new JPanel(){
                    @Override
                    public void paintComponent(Graphics g)   //Drawing the background
                    {
                        g.drawImage(backgroundGreen,0,0,500,550, null); //Setting the background image as background in the panel
                    }
                };
                dictionaryPanel.setSize(500,550);

                JTextArea text = new JTextArea();
                text.setLineWrap(true);
                text.setWrapStyleWord(true);
                text.setOpaque(false);
                text.setForeground(Color.LIGHT_GRAY);
                text.setBounds(10,10, 400, 280);
                text.setFont(new Font("Calibri", Font.PLAIN, 14));

                dictionaryPanel.add(text);

                f.add(dictionaryPanel);
                f.setVisible(true);

                text.append(r.getString("splitTitle") + "\n");
                text.append(r.getString("splitText") + "\n\n");
                text.append(r.getString("hitTitle") + "\n");
                text.append(r.getString("hitText") + "\n\n");
                text.append(r.getString("standTitle") + "\n");
                text.append(r.getString("standText") + "\n\n");
                text.append(r.getString("doubleTitle") + "\n");
                text.append(r.getString("doubleText") + "\n\n");
                text.append(r.getString("bustTitle") + "\n");
                text.append(r.getString("bustText"));
                text.setEditable(false);
            }
        });

        english.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               Login.updateGUI("en");
               updateGUI("en");
               if (CreateNewUser.created) {
                   CreateNewUser.updateGUI("en");
               }
               if (Home.created){
                   Home.updateGUI("en");
               }
               if (Multiplayer.created) {
                   Multiplayer.updateGUI("en");
               }
               if (Singleplayer.created){
                   Singleplayer.updateGUI("en");
               }
               lang = "en";
               CreateNewUser.lang = "en";
               Login.lang = "en";
               Home.lang = "en";
               Multiplayer.lang = "en";
               Singleplayer.lang = "en";
            }
        });

        norwegian.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Login.updateGUI("no");
                updateGUI("no");
                if (CreateNewUser.created) {
                    CreateNewUser.updateGUI("no");
                }
                if (Home.created) {
                    Home.updateGUI("no");
                }
                if (Multiplayer.created) {
                    Multiplayer.updateGUI("no");
                }
                if (Singleplayer.created) {
                    Singleplayer.updateGUI("no");
                }
                CreateNewUser.lang = "no";
                Login.lang = "no";
                Home.lang = "no";
                Multiplayer.lang = "no";
                Singleplayer.lang = "no";
                lang = "no";
            }
        });

        return menu;
    }
}