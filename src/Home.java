

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.*;
import java.util.Locale;
import java.util.ResourceBundle;

public class Home {
    static int balance;                            //Declare variables
    static String userName;
    static String password;
    static Player user;
    static JButton rules;
    static JButton multiPlayer;
    static JButton SinglePlayer;
    static JButton passwrdChange;
    static JButton logout;
    static JLabel showUsername;
    static JLabel showBalance;
    static JLabel header;
    static String lang = "en";
    static ResourceBundle home, rulesResource;
    static JFrame f;
    static Boolean created = false;
    public BufferedImage backgroundGreen;

    //Resizes icon to fit label
    private static ImageIcon resizeIcon(ImageIcon icon, int Width, int Height) {
        Image img = icon.getImage();                                                   //Makes image object
        Image resizedIcon = img.getScaledInstance(Width, Height, java.awt.Image.SCALE_SMOOTH);  //Scales image
        return new ImageIcon(resizedIcon);                                                      //returns resized imageobject
    }


    public void RunHome(Player player) {
        home = ResourceBundle.getBundle("home", Locale.forLanguageTag(lang));
        this.userName = player.getUserName();                 //Set variables from login
        this.balance = player.getBalance();
        this.password = player.getPassWord();
        this.user = player;

        DBConnect db = new DBConnect();


        ImageIcon backGroundimg = new ImageIcon(this.getClass().getResource("Resources/Blackjackimg2.jpg"));
        Font calibri = new Font("Calibri", Font.BOLD, 30);     //Declares new font
        Font buttons = new Font("Calibri", Font.BOLD, 20);     //Declares new font
        f = new JFrame(home.getString("mainmenu"));                        //Initialize Jframe with objects
        ImageIcon logoIcon = new ImageIcon(this.getClass().getResource("Resources/defaultIcon.png"));
        Image img = logoIcon.getImage();
        f.setIconImage(img);
        SinglePlayer = new JButton(home.getString("singleplayer"));
        SinglePlayer.setBackground(Color.LIGHT_GRAY);
        multiPlayer = new JButton(home.getString("multiplayer"));
        multiPlayer.setBackground(Color.LIGHT_GRAY);
        SinglePlayer.setBounds(300, 150, 200, 70);
        multiPlayer.setBounds(300, 230, 200, 70);
        SinglePlayer.setFont(buttons);
        multiPlayer.setFont(buttons);
        rules = new JButton(home.getString("rules"));
        rules.setBackground(Color.lightGray);
        rules.setBounds(300, 310, 200, 70);
        rules.setFont(buttons);
        header = new JLabel("Blackjack");
        header.setBounds(340, 80, 200, 70);
        header.setFont(calibri);
        header.setForeground(Color.LIGHT_GRAY);
        JLabel backGround = new JLabel();
        backGround.setBounds(0, 0, 800, 600);
        backGround.setIcon(backGroundimg);
        showUsername = new JLabel(home.getString("username") + ": " + userName);
        showUsername.setBounds(640, 420, 150, 30);
        showBalance = new JLabel(home.getString("balance")+ ": " + String.valueOf(balance));
        showBalance.setBounds(640, 440, 150, 30);
        passwrdChange = new JButton(home.getString("pwdchange"));
        passwrdChange.setBackground(Color.lightGray);
        passwrdChange.setBounds(640, 470, 150, 30);
        passwrdChange.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changePassword();
            }
        });
        logout = new JButton(home.getString("logout"));
        logout.setBackground(Color.lightGray);
        logout.setBounds(640, 505, 150, 30);
        logout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                db.removeUserFromLoggedInTable(userName);
                f.dispose();
                Login l = new Login();
                l.Login();
            }
        });
        if (userName.startsWith("Guest")) {       //If its a guest user
            passwrdChange.setEnabled(false);     //It cannot change the password
        }

        SinglePlayer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Singleplayer spGame = new Singleplayer();
                spGame.start(player);
                f.dispose();
            }
        });

        multiPlayer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Multiplayer mpGame = new Multiplayer(player);
                f.dispose();
            }
        });

        rules.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rules();
            }
        });


        JLayeredPane Layeredpane = new JLayeredPane();             //Make a new layeredpane
        Layeredpane.add(backGround, JLayeredPane.DEFAULT_LAYER);   //Add background behind the components

        Layeredpane.add(SinglePlayer, JLayeredPane.POPUP_LAYER);     //Add components in front of backgroud
        Layeredpane.add(multiPlayer, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(rules, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(header, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(showUsername, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(showBalance, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(logout, JLayeredPane.POPUP_LAYER);
        Layeredpane.add(passwrdChange, JLayeredPane.POPUP_LAYER);


        f.add(Layeredpane);                                         //Add the layeredpane to main frame
        f.setJMenuBar(Menubar.Menubar(f, lang, userName));                          //Sets menubar from manubar class
        f.setSize(800, 600);
        f.setVisible(true);
        f.setResizable(false);
        f.addWindowListener(new WindowAdapter() {          //Make sure that the user is removed from the loggedIn table when exiting
            @Override
            public void windowClosing(WindowEvent e) {
                db.removeUserFromLoggedInTable(userName);
            }
        });
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        if(balance<5){
            lowbalance();
        }

        created = true;
    }

    static void updateGUI(String s) {
        home = ResourceBundle.getBundle("home", Locale.forLanguageTag(s));
        SinglePlayer.setText(home.getString("singleplayer"));
        multiPlayer.setText(home.getString("multiplayer"));
        rules.setText(home.getString("rules"));
        showUsername.setText(home.getString("username") + ": " + userName);
        showBalance.setText(home.getString("balance")+ ": " + String.valueOf(balance));
        passwrdChange.setText(home.getString("pwdchange"));
        logout.setText(home.getString("logout"));
        f.setTitle(home.getString("mainmenu"));
    }

    private void changePassword(){                             //Creating a new frame where the user can change password
        JFrame changepsw = new JFrame(home.getString("pwdchange"));
        ImageIcon logoIcon = new ImageIcon(this.getClass().getResource("Resources/defaultIcon.png"));
        Image img = logoIcon.getImage();
        changepsw.setIconImage(img);
        changepsw.setSize(600,500);
        JLayeredPane Layeredpane2 = new JLayeredPane();            //new Layeredpane

        ImageIcon backGroundimg2 = new ImageIcon(this.getClass().getResource("Resources/background_green.jpg"));
        JLabel backGround = new JLabel();
        backGround.setBounds(0, 0, 600, 500);
        backGround.setIcon(backGroundimg2);

        Layeredpane2.add(backGround, JLayeredPane.DEFAULT_LAYER);   //Add background to the layeredpane, behind the other components

        JLabel changepswLabel = new JLabel(home.getString("pwdchange"));  //The title
        changepswLabel.setBackground(Color.lightGray);
        changepswLabel.setForeground(Color.LIGHT_GRAY);
        changepswLabel.setFont(new Font("Tahoma", Font.PLAIN, 36));
        changepswLabel.setBounds(50, 5, 400, 50);

        JLabel label = new JLabel("");                          //Label to insert messages
        label.setFont(new Font("Tahoma", Font.PLAIN, 16));
        label.setForeground(Color.LIGHT_GRAY);
        label.setBackground(Color.LIGHT_GRAY);
        label.setBounds(50, 50, 400, 50);

        JLabel oldpswLabel = new JLabel(home.getString("oldPassword"));     //Old password label
        oldpswLabel.setBackground(Color.lightGray);
        oldpswLabel.setForeground(Color.LIGHT_GRAY);
        oldpswLabel.setFont(new Font("Tahoma", Font.PLAIN, 27));
        oldpswLabel.setBounds(5, 130, 230, 50);

        JPasswordField oldpsw = new JPasswordField();            //Old password field
        oldpsw.setBounds(250,130,250, 50);

        JLabel newpswLabel = new JLabel(home.getString("newPassword"));   //New password label
        newpswLabel.setForeground(Color.LIGHT_GRAY);
        newpswLabel.setBackground(Color.lightGray);
        newpswLabel.setFont(new Font("Tahoma", Font.PLAIN, 27));
        newpswLabel.setBounds(5, 200, 230, 50);

        JPasswordField newpsw = new JPasswordField();           //New password field
        newpsw.setBounds(250,200,250, 50);

        JLabel newpswLabel2 = new JLabel(home.getString("newPassword"));  //New password label2
        newpswLabel2.setBackground(Color.LIGHT_GRAY);
        newpswLabel2.setForeground(Color.lightGray);
        newpswLabel2.setFont(new Font("Tahoma", Font.PLAIN, 27));
        newpswLabel2.setBounds(5, 270, 230, 50);

        JPasswordField newpsw2 = new JPasswordField();           //New password field2
        newpsw2.setBounds(250,270,250, 50);

        JButton changepswButton = new JButton(home.getString("pwdchange"));  //Button to change the password
        changepswButton.setBackground(Color.LIGHT_GRAY);
        changepswButton.setBounds(250,350, 250,50);
        changepswButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(oldpsw.getText().equals(password)){    //If the old password is correct
                    if(newpsw.getText().equals(newpsw2.getText())){   //If the two new passwords are equal
                        user.changePassword(newpsw.getText());       //Change the password to the new password
                        changepsw.dispose();
                    }else{                                           //Else if the two new passwords are not equal
                        label.setText(home.getString("newPasswordmsg"));
                    }

                }else{                                              //Else if the ols password is incorrect
                    label.setText(home.getString("wrongOldPasswordmsg"));
                }
            }
        });
        Layeredpane2.add(changepswLabel, JLayeredPane.POPUP_LAYER);   //Adding all the components to the layeredpane2
        Layeredpane2.add(oldpswLabel, JLayeredPane.POPUP_LAYER);
        Layeredpane2.add(oldpsw, JLayeredPane.POPUP_LAYER);
        Layeredpane2.add(newpswLabel, JLayeredPane.POPUP_LAYER);
        Layeredpane2.add(newpsw, JLayeredPane.POPUP_LAYER);
        Layeredpane2.add(newpswLabel2,JLayeredPane.POPUP_LAYER);
        Layeredpane2.add(newpsw2, JLayeredPane.POPUP_LAYER);
        Layeredpane2.add(label, JLayeredPane.POPUP_LAYER);
        Layeredpane2.add(changepswButton, JLayeredPane.POPUP_LAYER);

        changepsw.add(Layeredpane2);                                //Adding the layeredpane2 to the frame
        changepsw.setResizable(false);
        changepsw.setVisible(true);
    }

    private void rules(){
        rulesResource = ResourceBundle.getBundle("rules", Locale.forLanguageTag(lang));   //THe resource bundle for the rules frame
        JFrame rulesF = new JFrame(rulesResource.getString("title"));  //Creating a new frame that pops up with the rules
        ImageIcon logoIcon = new ImageIcon(this.getClass().getResource("Resources/defaultIcon.png"));
        Image img = logoIcon.getImage();
        rulesF.setIconImage(img);
        rulesF.setSize(600,500);

        try{
            backgroundGreen = ImageIO.read((this.getClass().getResource("Resources/background_green.jpg")));  //The background for the frame
        }catch(IOException ex){
            ex.printStackTrace();
        }

        JPanel rulesPanel = new JPanel (){
            @Override
            public void paintComponent(Graphics g)   //Drawing the background
            {
                g.drawImage(backgroundGreen,0,0,1000,5000, null); //Setting the background image as background in the panel
            }
        };
        rulesPanel.setLayout(new BoxLayout(rulesPanel, BoxLayout.Y_AXIS));  //Using boxlayout
        rulesPanel.setSize(500, 500);

        JLabel rulesTitle = new JLabel(rulesResource.getString("title"));   //The rules title
        rulesTitle.setBackground(Color.LIGHT_GRAY);
        rulesTitle.setForeground(Color.LIGHT_GRAY);
        rulesTitle.setFont(new Font("Tahoma", Font.BOLD, 36));
        rulesTitle.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextArea rulesIntro = new JTextArea(rulesResource.getString("intro"));  //Introduction to the rules
        rulesIntro.setLineWrap(true);
        rulesIntro.setWrapStyleWord(true);
        rulesIntro.setEditable(false);
        rulesIntro.setOpaque(false);
        rulesIntro.setForeground(Color.LIGHT_GRAY);
        rulesIntro.setFont(new Font("Tahoma", Font.PLAIN, 15));
        rulesIntro.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel deck = new JLabel(rulesResource.getString("deck"));   //The deck rules and explanations
        deck.setBackground(Color.LIGHT_GRAY);
        deck.setForeground(Color.LIGHT_GRAY);
        deck.setFont(new Font("Tahoma", Font.BOLD, 20));
        deck.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextArea deckText = new JTextArea(rulesResource.getString("deckText"));
        deckText.setLineWrap(true);
        deckText.setWrapStyleWord(true);
        deckText.setEditable(false);
        deckText.setOpaque(false);
        deckText.setForeground(Color.LIGHT_GRAY);
        deckText.setFont(new Font("Tahoma", Font.PLAIN, 15));
        deckText.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel bet = new JLabel(rulesResource.getString("bet"));   //The betting rules and explanations
        bet.setBackground(Color.LIGHT_GRAY);
        bet.setForeground(Color.LIGHT_GRAY);
        bet.setFont(new Font("Tahoma", Font.BOLD, 20));
        bet.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextArea betText = new JTextArea(rulesResource.getString("betText"));
        betText.setLineWrap(true);
        betText.setWrapStyleWord(true);
        betText.setEditable(false);
        betText.setOpaque(false);
        betText.setForeground(Color.LIGHT_GRAY);
        betText.setFont(new Font("Tahoma", Font.PLAIN, 15));
        betText.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel objectOTG = new JLabel(rulesResource.getString("objectOTG"));  //The point of the game
        objectOTG.setBackground(Color.LIGHT_GRAY);
        objectOTG.setForeground(Color.LIGHT_GRAY);
        objectOTG.setFont(new Font("Tahoma", Font.BOLD, 20));
        objectOTG.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextArea objectOTGText = new JTextArea(rulesResource.getString("objectOTGText"));
        objectOTGText.setLineWrap(true);
        objectOTGText.setWrapStyleWord(true);
        objectOTGText.setEditable(false);
        objectOTGText.setOpaque(false);
        objectOTGText.setForeground(Color.LIGHT_GRAY);
        objectOTGText.setFont(new Font("Tahoma", Font.PLAIN, 15));
        objectOTGText.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel cardValues = new JLabel(rulesResource.getString("cardValues"));  //Card values and explanations
        cardValues.setBackground(Color.LIGHT_GRAY);
        cardValues.setForeground(Color.LIGHT_GRAY);
        cardValues.setFont(new Font("Tahoma", Font.BOLD, 20));
        cardValues.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextArea cardValuesText = new JTextArea(rulesResource.getString("cardValuesText"));
        cardValuesText.setLineWrap(true);
        cardValuesText.setWrapStyleWord(true);
        cardValuesText.setEditable(false);
        cardValuesText.setOpaque(false);
        cardValuesText.setForeground(Color.LIGHT_GRAY);
        cardValuesText.setFont(new Font("Tahoma", Font.PLAIN, 15));
        cardValuesText.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel theDeal = new JLabel(rulesResource.getString("theDeal"));  //The deal rules and explanations
        theDeal.setBackground(Color.LIGHT_GRAY);
        theDeal.setForeground(Color.LIGHT_GRAY);
        theDeal.setFont(new Font("Tahoma", Font.BOLD, 20));
        theDeal.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextArea theDealText = new JTextArea(rulesResource.getString("theDealText"));
        theDealText.setLineWrap(true);
        theDealText.setWrapStyleWord(true);
        theDealText.setEditable(false);
        theDealText.setOpaque(false);
        theDealText.setForeground(Color.LIGHT_GRAY);
        theDealText.setFont(new Font("Tahoma", Font.PLAIN, 15));
        theDealText.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel blackjack = new JLabel(rulesResource.getString("blackjack"));  //Blackjack explanation
        blackjack.setBackground(Color.LIGHT_GRAY);
        blackjack.setForeground(Color.LIGHT_GRAY);
        blackjack.setFont(new Font("Tahoma", Font.BOLD, 20));
        blackjack.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextArea blackjackText = new JTextArea(rulesResource.getString("blackjackText"));
        blackjackText.setLineWrap(true);
        blackjackText.setWrapStyleWord(true);
        blackjackText.setEditable(false);
        blackjackText.setOpaque(false);
        blackjackText.setForeground(Color.LIGHT_GRAY);
        blackjackText.setFont(new Font("Tahoma", Font.PLAIN, 15));
        blackjackText.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel thePlay = new JLabel(rulesResource.getString("thePlay"));  //The play rules and explanations
        thePlay.setBackground(Color.LIGHT_GRAY);
        thePlay.setForeground(Color.LIGHT_GRAY);
        thePlay.setFont(new Font("Tahoma", Font.BOLD, 20));
        thePlay.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextArea thePlayText = new JTextArea(rulesResource.getString("thePlayText"));
        thePlayText.setLineWrap(true);
        thePlayText.setWrapStyleWord(true);
        thePlayText.setEditable(false);
        thePlayText.setOpaque(false);
        thePlayText.setForeground(Color.LIGHT_GRAY);
        thePlayText.setFont(new Font("Tahoma", Font.PLAIN, 15));
        thePlayText.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel theDealersPlay = new JLabel(rulesResource.getString("theDealersPlay"));  //The dealers play rules and explanations
        theDealersPlay.setBackground(Color.LIGHT_GRAY);
        theDealersPlay.setForeground(Color.LIGHT_GRAY);
        theDealersPlay.setFont(new Font("Tahoma", Font.BOLD, 20));
        theDealersPlay.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextArea theDealersPlayText = new JTextArea(rulesResource.getString("theDealersPlayText"));
        theDealersPlayText.setLineWrap(true);
        theDealersPlayText.setWrapStyleWord(true);
        theDealersPlayText.setEditable(false);
        theDealersPlayText.setOpaque(false);
        theDealersPlayText.setForeground(Color.LIGHT_GRAY);
        theDealersPlayText.setFont(new Font("Tahoma", Font.PLAIN, 15));
        theDealersPlayText.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel splittingPairs = new JLabel(rulesResource.getString("splittingPairs"));  //Splitting explanations
        splittingPairs.setBackground(Color.LIGHT_GRAY);
        splittingPairs.setForeground(Color.LIGHT_GRAY);
        splittingPairs.setFont(new Font("Tahoma", Font.BOLD, 20));
        splittingPairs.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextArea splittingPairsText = new JTextArea(rulesResource.getString("splittingPairsText"));
        splittingPairsText.setLineWrap(true);
        splittingPairsText.setWrapStyleWord(true);
        splittingPairsText.setEditable(false);
        splittingPairsText.setOpaque(false);
        splittingPairsText.setForeground(Color.LIGHT_GRAY);
        splittingPairsText.setFont(new Font("Tahoma", Font.PLAIN, 15));
        splittingPairsText.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel doublingDown = new JLabel(rulesResource.getString("doublingDown"));   //Doubling down explanations
        doublingDown.setBackground(Color.LIGHT_GRAY);
        doublingDown.setForeground(Color.LIGHT_GRAY);
        doublingDown.setFont(new Font("Tahoma", Font.BOLD, 20));
        doublingDown.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextArea doublingDownText = new JTextArea(rulesResource.getString("doublingDownText"));
        doublingDownText.setLineWrap(true);
        doublingDownText.setWrapStyleWord(true);
        doublingDownText.setEditable(false);
        doublingDownText.setOpaque(false);
        doublingDownText.setForeground(Color.LIGHT_GRAY);
        doublingDownText.setFont(new Font("Tahoma", Font.PLAIN, 15));
        doublingDownText.setAlignmentX(Component.CENTER_ALIGNMENT);


        rulesPanel.add(rulesTitle);  //adding all the texts and labels to the panel
        rulesPanel.add(rulesIntro);
        rulesPanel.add(deck);
        rulesPanel.add(deckText);
        rulesPanel.add(bet);
        rulesPanel.add(betText);
        rulesPanel.add(objectOTG);
        rulesPanel.add(objectOTGText);
        rulesPanel.add(cardValues);
        rulesPanel.add(cardValuesText);
        rulesPanel.add(theDeal);
        rulesPanel.add(theDealText);
        rulesPanel.add(blackjack);
        rulesPanel.add(blackjackText);
        rulesPanel.add(thePlay);
        rulesPanel.add(thePlayText);
        rulesPanel.add(theDealersPlay);
        rulesPanel.add(theDealersPlayText);
        rulesPanel.add(splittingPairs);
        rulesPanel.add(splittingPairsText);
        rulesPanel.add(doublingDown);
        rulesPanel.add(doublingDownText);



        JScrollPane rulesScroll = new JScrollPane(rulesPanel);  //Making the panel a scrollable panel
        SwingUtilities.invokeLater(new Runnable()
        {                                                       //To make the scrollPanel start at the top
            public void run()
            {
                rulesScroll.getViewport().setViewPosition( new Point(0, 0) );
            }
        });



        rulesF.add(rulesScroll);  //Adding the panel to the frame
        rulesF.setVisible(true);
        rulesF.setResizable(false);
    }

    private void lowbalance(){
        JFrame lowBframe = new JFrame();   //New frame to pop up when the user does not have enough balance to play
        ImageIcon logoIcon = new ImageIcon(this.getClass().getResource("Resources/defaultIcon.png"));
        Image img = logoIcon.getImage();
        lowBframe.setIconImage(img);
        lowBframe.setSize(550, 480);

        try{    //Reading the background image
            backgroundGreen = ImageIO.read((this.getClass().getResource("Resources/background_green.jpg")));  //The background for the frame
        }catch(IOException ex){
            ex.printStackTrace();
        }

        JPanel lowBPanel = new JPanel (){
            @Override
            public void paintComponent(Graphics g)   //Drawing the background
            {  //Creating a panel to have all the components
                g.drawImage(backgroundGreen,0,0,550,480, null); //Setting the background image as background in the panel
            }
        };
        lowBPanel.setLayout(new BoxLayout(lowBPanel, BoxLayout.Y_AXIS));  //Using boxlayout
        lowBPanel.setSize(550, 480);

        JLabel title = new JLabel(home.getString("whoops"));   //The title
        title.setBackground(Color.LIGHT_GRAY);
        title.setForeground(Color.LIGHT_GRAY);
        title.setFont(new Font("Tahoma", Font.BOLD, 19));
        title.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel label2 = new JLabel(home.getString("luckily"));   //New label for the options
        label2.setBackground(Color.LIGHT_GRAY);
        label2.setForeground(Color.LIGHT_GRAY);
        label2.setFont(new Font("Tahoma", Font.PLAIN, 16));
        label2.setAlignmentX(Component.CENTER_ALIGNMENT);

        Dimension d = new Dimension(350, 55);    //Dumension of the buttons

        JButton weddingRing = new JButton(home.getString("weddingRing"));  //Selling the weddingring button
        weddingRing.setFont(new Font("Tahoma", Font.PLAIN, 17));
        weddingRing.setMinimumSize(d);
        weddingRing.setMaximumSize(d);
        weddingRing.setPreferredSize(d);
        weddingRing.setAlignmentX(Component.CENTER_ALIGNMENT);
        weddingRing.setBackground(Color.LIGHT_GRAY);
        weddingRing.addActionListener(new ActionListener() {   //Adding the new value to the balance
            @Override
            public void actionPerformed(ActionEvent e) {
                lowBframe.dispose();
                user.changeBalance((balance+=5000));
                showBalance.setText(home.getString("balance")+ ": " + String.valueOf(balance));

            }
        });

        JButton car = new JButton(home.getString("car"));              //Selling the car button
        car.setFont(new Font("Tahoma", Font.PLAIN, 17));
        car.setMinimumSize(d);
        car.setMaximumSize(d);
        car.setPreferredSize(d);
        car.setAlignmentX(Component.CENTER_ALIGNMENT);
        car.setBackground(Color.LIGHT_GRAY);
        car.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {   //Adding the new value to the balance
                lowBframe.dispose();
                user.changeBalance((balance+=10000));
                showBalance.setText(home.getString("balance")+ ": " + String.valueOf(balance));

            }
        });

        JButton friend = new JButton(home.getString("friend"));    //Borrowing from a friend button
        friend.setFont(new Font("Tahoma", Font.PLAIN, 16));
        friend.setMinimumSize(d);
        friend.setMaximumSize(d);
        friend.setPreferredSize(d);
        friend.setAlignmentX(Component.CENTER_ALIGNMENT);
        friend.setBackground(Color.LIGHT_GRAY);
        friend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {   //Adding the new value to the balance
                lowBframe.dispose();
                user.changeBalance((balance+=2000));
                showBalance.setText(home.getString("balance")+ ": " + String.valueOf(balance));

            }
        });
                                                                                   //Text with the disclaimer
        JTextArea warning = new JTextArea(home.getString("disclaimer"));
        warning.setLineWrap(true);
        warning.setWrapStyleWord(true);
        warning.setEditable(false);
        Dimension d2 = new Dimension(470, 100);    //Dimension of the warning text
        warning.setMinimumSize(d2);
        warning.setMaximumSize(d2);
        warning.setPreferredSize(d2);
        warning.setOpaque(false);
        warning.setForeground(Color.LIGHT_GRAY);
        warning.setFont(new Font("Tahoma", Font.PLAIN, 10));
        warning.setAlignmentX(Component.CENTER_ALIGNMENT);

        lowBPanel.add(Box.createRigidArea(new Dimension(0,10)));  //Adding the components to the panel
        lowBPanel.add(title);
        lowBPanel.add(Box.createRigidArea(new Dimension(0,50)));
        lowBPanel.add(label2);
        lowBPanel.add(Box.createRigidArea(new Dimension(0,10)));
        lowBPanel.add(friend);
        lowBPanel.add(Box.createRigidArea(new Dimension(0,10)));
        lowBPanel.add(weddingRing);
        lowBPanel.add(Box.createRigidArea(new Dimension(0,10)));
        lowBPanel.add(car);
        lowBPanel.add(Box.createRigidArea(new Dimension(0,80)));
        lowBPanel.add(warning);

        lowBframe.add(lowBPanel);
        lowBframe.setVisible(true);
        lowBframe.setResizable(false);
    }


}
