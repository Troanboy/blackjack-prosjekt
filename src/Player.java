import java.lang.*;
import java.sql.*;
//import java.net.*;

public class Player extends Thread{
    private String userName;
    private String passWord;
    private int balance;
    private int currentBet;
    private int handValue;
    static Boolean bust = false;
    private DBConnect db;

    public Player(){}

    public Player(String userName, String passWord, int balance){
        this.userName = userName;
        this.passWord = passWord;
        this.balance = balance;
        this.currentBet = 0;
        this.handValue = 0;
        db = new DBConnect();
    }

    public void setUserName(String userName){
        this.userName = userName;
    }

    public void setPassWord(String passWord){
        this.passWord = passWord;
    }

    public void setBalance(int bal){
        this.balance = bal;
    }

    public void setCurrentBet(int cBet){
        this.currentBet = cBet;
    }

    public void setHandValue(int hValue){
        this.handValue = hValue;
    }

    public String getUserName(){
        return this.userName;
    }

    public String getPassWord(){
        return this.passWord;
    }

    public int getBalance(){
        return this.balance;
    }

    public int getCurrentBet(){
        return this.currentBet;
    }

    public int getHandValue(){
        return handValue;
    }

    public void reset(){
        handValue = 0;
        currentBet = 0;
        bust = false;
    }

    /*creates a new user/adds this user to database
    public void createUser(){
        //sql statement:
        //INSERT INTO Players(username, password, balance) VALUES (this.userName, this.passWord, this.balance)

    }

    public void joinGame(){

    }*/

    public void changeBalance(int newBalance) {
        balance = newBalance;
        db.changeBalance(userName, balance);
    }

    public void changePassword(String newPassword){
        passWord = newPassword;
        db.changePassword(userName, passWord);
    }

}
