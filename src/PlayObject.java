import java.io.Serializable;

public class PlayObject implements Serializable {

    int ID;
    int hand;
    int currentBet = 0;
    String purpose;
    Card card;
    String username;

    public PlayObject(String userName, Card theCard, int playerNumber, int handVal, int bet, String playType) {
        card = theCard;
        ID = playerNumber;
        purpose = playType;
        hand = handVal;
        currentBet = bet;
        username = userName;
    }

    public Card getCard() {
        return card;
    }

    public int getID() {
        return ID;
    }

    public String getPurpose() { return purpose; }

    public int getHand() {return hand;}

    public int getBet() {return currentBet;}

    public String getUsername(){return username;}
}
