INFORMATION ABOUT MULTIPLAYER

- In order to play multiplayer, you must first run the Server.jar file.
- Maximum numbers of players in multiplayer are 3.
- After the Server.jar file is run, any player can now press the multiplayer button.
- After the first player joins multiplayer, the server will start a countdown from 15 seconds, where all the rest of the players must join the 
game before the countdown is finished. The game will start when the countdown is finished.
- If a player fails to join the game before the countdown ends, the player will not be able to join that multiplayer session. 
- If a player chooses to leave the game, the game will still run for the rest of the players, but the player who left will not be able to join that session again. 
- If all players leave the game, the server will restart automatically, and start a new countdown when the first player joins. 
 